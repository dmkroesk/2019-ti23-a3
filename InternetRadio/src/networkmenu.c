/*
 * networkmenu.c
 *
 * Created: 19/03/2019 17:15:17
 * Author: Sergen
 */

#define LOG_MODULE LOG_MAIN_MODULE
#define AMOUNTOFVALUES 12

#include "draw.h"
#include "alarm.h"
#include "log.h"
#include "keylogic.h"
#include "radio.h"
#include "networkmenu.h"
#include "storage.h"


int networkElement = 0;
int timesyncElement = 0;
char *networkElements[2];
char *timesyncElements[2];

int macCursorPos = 0;
unsigned int values[AMOUNTOFVALUES];


void InitNetworkMenuElements()
{
	networkElements[0] = ("<  MAC-adres  >");
	networkElements[1] = ("<  Tijd sync. >");
}

void InitMacSettings()
{
	Flash_MAC mac;
	ReadMACFromFlash(&mac);
	
	int i = 0;
	for(; i < AMOUNTOFVALUES; i++)
		values[i] = mac.data[i];
	
	macCursorPos = 0;
	
	SetMacScreen();
}

void InitTimeSync()
{
	Flash_TimeSync timezone;
	ReadTimeSyncFromFlash(&timezone);
	timesyncElement = timezone.synched;
	
	timesyncElements[0] = (" Ja       [Nee]");
	timesyncElements[1] = ("[Ja]       Nee ");
}

void NetworkMenuLeft()
{
	networkElement--;
	if(networkElement < 0)
		networkElement = 1;
	
	SetNetworkMenuElement();
}

void NetworkMenuRight()
{
	networkElement++;
	if(networkElement > 1)
		networkElement = 0;
	
	SetNetworkMenuElement();
}

void MacLeft()
{
	if(macCursorPos <= 0)
		return;
	
	macCursorPos -= 1;
	
	SetMacCursor();
	
	if(macCursorPos == 5)
	{
		SetScreenLine1("MAC        v1-3");
		SetMacScreen();
	}
}

void MacRight()
{
	if (macCursorPos >= AMOUNTOFVALUES-1)
		return;
	
	macCursorPos += 1;
	
	SetMacCursor();
	
	if (macCursorPos == 6)
	{
		SetScreenLine1("MAC v       4-6");
		SetMacScreen();
	}
}

void MacUp()
{
	if(values[macCursorPos] < 15)
		values[macCursorPos] += 1;
	
	SetMacScreen();
}

void MacDown()
{
	if(values[macCursorPos] > 0)
		values[macCursorPos] -= 1;
	
	SetMacScreen();
}

void TimeSyncMenuLeft()
{
	timesyncElement--;
	if(timesyncElement < 0)
		timesyncElement = 1;
	
	SetTimeSyncMenuElement();
}

void TimeSyncMenuRight()
{
	timesyncElement++;
	if(timesyncElement > 1)
		timesyncElement = 0;
	
	SetTimeSyncMenuElement();
}

void SelectNetworkItem()
{
	switch (networkElement)
	{
		case 0:
			ToMAC();
			break;
		case 1:
			ToTimeSync();
			break;
	}
}

void SetNetworkMenuElement()
{
	SetScreenLine2(networkElements[networkElement]);
}

void SetTimeSyncMenuElement()
{
	SetScreenLine2(timesyncElements[timesyncElement]);
}

void SetMacCursor()
{
	if(macCursorPos == 0 || macCursorPos == 6)
		SetScreenLine1("MAC v       ");
	else if (macCursorPos == 1 || macCursorPos == 7)
		SetScreenLine1("MAC  v      ");
	else if (macCursorPos == 2 || macCursorPos == 8)
		SetScreenLine1("MAC    v    ");
	else if (macCursorPos == 3 || macCursorPos == 9)
		SetScreenLine1("MAC     v   ");
	else if (macCursorPos == 4 || macCursorPos == 10)
		SetScreenLine1("MAC       v ");
	else if (macCursorPos == 5 || macCursorPos == 11)
		SetScreenLine1("MAC        v");
}

void SetMacScreen()
{
	char *mac = malloc(sizeof(char)*10);
	
	if(macCursorPos <= 5)
		sprintf(mac, "    %x%x:%x%x:%x%x  >", values[0],values[1],values[2], values[3], values[4], values[5]);
	else
		sprintf(mac, "<   %x%x:%x%x:%x%x   ", values[6],values[7],values[8], values[9], values[10], values[11]);
	
	SetScreenLine2(mac);
	
	free(mac);
	
	DrawScreen();
}

void ChangeMacAddress()
{
	Flash_MAC mac;
	
	int i = 0;
	for(; i < 12; i++)
		mac.data[i] = values[i];
	
	WriteMACToFlash(mac);
	
	ToMenu();
}

void GetMACAddress(unsigned int *macAdress)
{
	int i = 0;
	for(; i < 6; i++)
		macAdress[i] = (values[i*2] * 16) + values[i+1];
}

void TimeSyncConfirm()
{
	Flash_TimeSync timezone;
	ReadTimeSyncFromFlash(&timezone);
	
	timezone.synched = timesyncElement;
	
	WriteTimeSyncToFlash(timezone);

	if (GetSyncStatus() == 1)
		SyncTime();
	
	networkElement = 1;
	
	ToNetwork();
}
