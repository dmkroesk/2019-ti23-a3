/*
* keybinding.c
*
* Created: 18/02/2019 20:00:19
* Author: Ian
*/

#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include <string.h>
#include <avr/delay.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <time.h>

#include "system.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "draw.h"
#include "rtc.h"
#include "x1205.h"
#include "keybinding.h"
#include "datesetting.h"
#include "timesetting.h"
#include "settings.h"
#include "alarmmenu.h"
#include "alarmsetting.h"
#include "keylogic.h"
#include "networkmenu.h"
#include "keyboard.h"
#include "states.h"
#include "weekalarms.h"
#include "weekday.h"
#include "soundcontroller.h"


struct Pair buttons[13];


void InitButtons()
{
	//Left panel
	buttons[0].key = KEY_01;
	buttons[0].value = SwitchRadio;
	buttons[1].key = KEY_02;
	buttons[1].value = SwitchAlarm;
	buttons[2].key = KEY_03;
	buttons[2].value = NULL;
	buttons[3].key = KEY_04;
	buttons[3].value = NULL;
	buttons[4].key = KEY_05;
	buttons[4].value = NULL;
	buttons[5].key = KEY_ALT;
	buttons[5].value = NULL;

	//Right panel
	buttons[6].key = KEY_ESC;
	buttons[6].value = NULL;
	buttons[7].key = KEY_UP;
	buttons[7].value = NULL;
	buttons[8].key = KEY_OK;
	buttons[8].value = NULL;
	buttons[9].key = KEY_LEFT;
	buttons[9].value = NULL;
	buttons[10].key = KEY_DOWN;
	buttons[10].value = NULL;
	buttons[11].key = KEY_RIGHT;
	buttons[11].value = NULL;

	//Power button
	buttons[12].key = KEY_POWER;
	buttons[12].value = TurnOff;
}

void SetButtons(int state)
{
	switch(state)
	{
		case IMC_STATE_OFF:
			//Left panel
			buttons[0].value = NULL;
			buttons[2].value = NULL;
			buttons[3].value = NULL;
			buttons[4].value = NULL;
			buttons[5].value = NULL;
			
			//Right panel
			buttons[6].value = NULL;
			buttons[7].value = NULL;
			buttons[8].value = NULL;
			buttons[9].value = NULL;
			buttons[10].value = NULL;
			buttons[11].value = NULL;
			
			//Power button
			buttons[12].value = ToMain;
			break;
		
		case IMC_STATE_MAIN:	
			//Left panel
			buttons[0].value = SwitchRadio;
			buttons[1].value = SwitchAlarm;
			buttons[2].value = NULL;
			buttons[3].value = NULL;
			buttons[4].value = NULL;
			buttons[5].value = NULL;
			
			//Right panel
			buttons[6].value = ToMenu;
			buttons[7].value = ToVolume;
			buttons[8].value = NULL;
			buttons[9].value = SwitchChannelLeft;
			buttons[10].value = ToVolume;
			buttons[11].value = SwitchChannelRight;
			
			//Power button
			buttons[12].value = TurnOff;
			break;
			
		case IMC_STATE_MENU:
			//Right panel
			buttons[6].value = ToMain;
			buttons[7].value = NULL;
			buttons[8].value = SelectItem;
			buttons[9].value = MenuLeft;
			buttons[10].value = NULL;
			buttons[11].value = MenuRight;
			break;
		
		case IMC_STATE_IDLE:
			buttons[0].value = Awake;
			buttons[2].value = Awake;
			buttons[3].value = Awake;
			buttons[4].value = Awake;
			buttons[5].value = Awake;
			//Right panel
			buttons[6].value = Awake;
			buttons[7].value = Awake;
			buttons[8].value = Awake;
			buttons[9].value = Awake;
			buttons[10].value = Awake;
			buttons[11].value = Awake;
			break;
		
		case IMC_STATE_ALARM:
			//Left panel
			buttons[0].value = AlarmSnooze;
			buttons[1].value = AlarmSnooze;
			buttons[2].value = AlarmSnooze;
			buttons[3].value = AlarmSnooze;
			buttons[4].value = AlarmSnooze;
			buttons[5].value = AlarmSnooze;
			//Right panel
			buttons[6].value = AlarmDone;
			buttons[7].value = AlarmDone;
			buttons[8].value = AlarmDone;
			buttons[9].value = AlarmDone;
			buttons[10].value = AlarmDone;
			buttons[11].value = AlarmDone;
			break;
			
		case IMC_STATE_MENU_TIME:
			//Right panel
			buttons[6].value = ToMenu;
			buttons[7].value = TimeSettingUp;
			buttons[8].value = TimeSettingConfirm;
			buttons[9].value = TimeSettingLeft;
			buttons[10].value = TimeSettingDown;
			buttons[11].value = TimeSettingRight;
			break;
			
		case IMC_STATE_MENU_DATE:
			//Right panel
			buttons[6].value = ToMenu;
			buttons[7].value = DateUp;
			buttons[8].value = ChangeDate;
			buttons[9].value = DateLeft;
			buttons[10].value = DateDown;
			buttons[11].value = DateRight;
			break;
			
		case IMC_STATE_MENU_SETTINGS:
			//Right panel
			buttons[6].value = ToMenu;
			buttons[7].value = NULL;
			buttons[8].value = SelectSettingsItem;
			buttons[9].value = SettingsLeft;
			buttons[10].value = NULL;
			buttons[11].value = SettingsRight;
			break;
			
		case IMC_STATE_MENU_NETWORK:
			buttons[6].value = ToMenu;
			buttons[7].value = NULL;
			buttons[8].value = SelectNetworkItem;
			buttons[9].value = NetworkMenuLeft;
			buttons[10].value = NULL;
			buttons[11].value = NetworkMenuLeft;
			break;
			
		case IMC_STATE_MENU_NETWORK_MAC:
			buttons[6].value = ToNetwork;
			buttons[7].value = MacUp;
			buttons[8].value = ChangeMacAddress;
			buttons[9].value = MacLeft;
			buttons[10].value = MacDown;
			buttons[11].value = MacRight;
			break;
			
		case IMC_STATE_MENU_NETWORK_TIMESYNC:
			buttons[6].value = ToNetwork;
			buttons[7].value = NULL;
			buttons[8].value = TimeSyncConfirm;
			buttons[9].value = TimeSyncMenuLeft;
			buttons[10].value = NULL;
			buttons[11].value = TimeSyncMenuRight;
			break;

		case IMC_STATE_MENU_ALARM_TIME:
			//Right panel
			buttons[6].value = ToAlarmMenu;
			buttons[7].value = AlarmSettingUp;
			buttons[8].value = AlarmSettingConfirm;
			buttons[9].value = AlarmSettingLeft;
			buttons[10].value = AlarmSettingDown;
			buttons[11].value = AlarmSettingRight;
			break;

		case IMC_STATE_MENU_ALARM_REARM:
			//Right panel
			buttons[6].value = ToAlarmMenu;
			buttons[7].value = NULL;
			buttons[8].value = ItemConfirm;
			buttons[9].value = ReArmOnOff;
			buttons[10].value = NULL;
			buttons[11].value = ReArmOnOff;
			break;

		case IMC_STATE_MENU_ALARM_SOUND:
			//Right panel
			buttons[6].value = ToAlarmMenu;
			buttons[7].value = NULL;
			buttons[8].value = ItemConfirm;
			buttons[9].value = RadioOnOff;
			buttons[10].value = NULL;
			buttons[11].value = RadioOnOff;
			break;
		
		case IMC_STATE_MENU_ALARM:
			//Right panel
			buttons[6].value = ToMenu;
			buttons[7].value = NULL;
			buttons[8].value = SelectAlarmItem;
			buttons[9].value = AlarmMenuLeft;
			buttons[10].value = NULL;
			buttons[11].value = AlarmMenuRight;
			break;
				
		case IMC_STATE_MENU_ALARM_RADIO:
			//Right panel
			buttons[6].value = ToAlarmMenu;
			buttons[7].value = NULL;
			buttons[8].value = SelectRadioItem;
			buttons[9].value = AlarmRadioMenuLeft;
			buttons[10].value = NULL;
			buttons[11].value = AlarmRadioMenuRight;
			break;
				
		case IMC_STATE_MENU_SETTINGS_FACTORY:
			//Right panel
			buttons[6].value = ToSettings;
			buttons[7].value = NULL;
			buttons[8].value = FactoryReset;
			buttons[9].value = NULL;
			buttons[10].value = NULL;
			buttons[11].value = NULL;
			break;
		case IMC_STATE_MENU_SETTINGS_TIMEZONE:
			//Right panel
			buttons[6].value = ToSettings;
			buttons[7].value = NULL;
			buttons[8].value = SelectTimezoneItem;
			buttons[9].value = TimezonesLeft;
			buttons[10].value = NULL;
			buttons[11].value = TimezonesRight;
			break;
		case IMC_STATE_MENU_ALARM_WEEK:
			//Right panel
			buttons[6].value = ToAlarmMenu;
			buttons[7].value = NULL;
			buttons[8].value = WeekAlarm_SelectItem;
			buttons[9].value = WeekAlarm_Left;
			buttons[10].value = NULL;
			buttons[11].value = WeekAlarm_Right;
			break;
		
		case IMC_STATE_MENU_ALARM_WEEK_DAY:
			//Right panel
			buttons[6].value = ToWeekAlarm;
			buttons[7].value = Weekday_Up;
			buttons[8].value = ConfirmWeekDaySettings;
			buttons[9].value = Weekday_Left;
			buttons[10].value = Weekday_Down;
			buttons[11].value = Weekday_Right;
			break;
			
		case IMC_STATE_MENU_AUDIO:
			//Left panel
			buttons[0].value = NULL;
			buttons[1].value = NULL;
			buttons[2].value = NULL;
			buttons[3].value = NULL;
			buttons[4].value = NULL;
			buttons[5].value = NULL;
		
			//Right panel
			buttons[6].value = ToMain;
			buttons[7].value = NULL;
			buttons[8].value = SelectAudioItem;
			buttons[9].value = AudioLeft;
			buttons[10].value = NULL;
			buttons[11].value = AudioRight;
			break;
		
		case IMC_STATE_MENU_AUDIO_VOLUME:
			//Left panel
			buttons[0].value = NULL;
			buttons[1].value = NULL;
			buttons[2].value = NULL;
			buttons[3].value = NULL;
			buttons[4].value = NULL;
			buttons[5].value = NULL;
		
			//Right panel
			buttons[6].value = ToMain;
			buttons[7].value = VolumeUp;
			buttons[8].value = NULL;
			buttons[9].value = NULL;
			buttons[10].value = VolumeDown;
			buttons[11].value = NULL;
			break;
		
		case IMC_STATE_MENU_AUDIO_BASS:
			//Left panel
			buttons[0].value = NULL;
			buttons[1].value = NULL;
			buttons[2].value = NULL;
			buttons[3].value = NULL;
			buttons[4].value = NULL;
			buttons[5].value = NULL;
		
			//Right panel
			buttons[6].value = ToAudioMenu;
			buttons[7].value = BassUp;
			buttons[8].value = NULL;
			buttons[9].value = NULL;
			buttons[10].value = BassDown;
			buttons[11].value = NULL;
			break;
		
		case IMC_STATE_MENU_AUDIO_TREBLE:
			//Left panel
			buttons[0].value = NULL;
			buttons[1].value = NULL;
			buttons[2].value = NULL;
			buttons[3].value = NULL;
			buttons[4].value = NULL;
			buttons[5].value = NULL;
		
			//Right panel
			buttons[6].value = ToAudioMenu;
			buttons[7].value = TrebleUp;
			buttons[8].value = NULL;
			buttons[9].value = NULL;
			buttons[10].value = TrebleDown;
			buttons[11].value = NULL;
			break;
	}
}

void *GetValue(u_char key)
{
	//Works like a map: Gets the value based on the given key
	int i = 0;
	for (; i < sizeof(buttons) / sizeof(struct Pair); i++)
	{
		if (buttons[i].key == key)
			return buttons[i].value;
	}
	
	return NULL;
}
