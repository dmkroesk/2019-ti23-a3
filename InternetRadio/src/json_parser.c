/*
* json_parser.c
*
* Created: 3/21/2019 3:24:38 PM
*  Author: Ruben
*/
#define LOG_MODULE LOG_MAIN_MODULE

#include "json_parser.h"
#include <string.h>
#include <stddef.h>
#include "log.h"

int ParseJSON(char *data, char *leftDelimiter, char *rightDelimiter, char **out)
{
	char *beginning = strstr(data, leftDelimiter);
	if (beginning == NULL)
	return 0;

	char *end = strstr(data, rightDelimiter);
	if (end == NULL)
	return 0;

	beginning += strlen(leftDelimiter);

	ptrdiff_t segmentLength = end - beginning;

	*out = malloc(segmentLength + 1);
	strncpy(*out, beginning, segmentLength);
	(*out)[segmentLength] = 0;

	return 1;
}

static void substr(char *str, char *sub, int start, int len)
{
	memcpy(sub, &str[start], len);
	sub[len] = '\0';
}

char *find_string_value_in_string(char *key, char *str, int length)
{
	char *k = strstr(str, key);
	int pos = (k - str) + strlen(key) + 2;
	int len = 0;
while (str[pos] != "," || str[pos] != "}")
{
	len++;
}
LogMsg_P(LOG_INFO, PSTR("Length for value from: %s is %d"), key, len);

char *sub = malloc(sizeof(char) * len);

substr(str, sub, pos, len);
return sub;
}

int find_int_value_in_string(char *key, char *str, int length)
{
	char *asString = "11"; //find_string_value_in_string(key, str, length);
	int num = atoi(asString);
	free(asString);
	return num;
}
