/*
 * alarmsetting.c
 *
 * Created: 28-2-2019 11:34:02
 * Author: Ricky
 */ 

#define LOG_MODULE LOG_MAIN_MODULE

#include <stdio.h>
#include <string.h>
#include <avr/delay.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "display.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "flash.h"
#include "rtc.h"
#include "datetime.h"
#include "watchdog.h"
#include "x1205.h"
#include "remcon.h"
#include "uart0driver.h"
#include "mmc.h"
#include "spidrv.h"
#include "draw.h"
#include "timesetting.h"
#include "alarm.h"
#include "storage.h"
#include "alarmsetting.h"
#include "keylogic.h"


int alarmPosition;
int alarmSetter[4];


static void Check(void);
static void RedrawAlarm(void);


void InitAlarmSetting()
{
	alarmPosition = 0;
	
	int i = 0;
	for(; i < 4; i++)
	{
		alarmSetter[i] = 0;
	}
				
	RedrawAlarm();
}
	
static void Check()
{
	int i = 0;
	for (; i < 4; i++)
	{
		if (alarmSetter[i] > 9)
			alarmSetter[i] = 0;
	}
		
	if (Concatenate(alarmSetter[0], alarmSetter[1]) > 23)
	{
		alarmSetter[0] = 0;
		alarmSetter[1] = 0;
	}
	if (Concatenate(alarmSetter[2], alarmSetter[3]) > 59)
	{
		alarmSetter[2] = 0;
		alarmSetter[3] = 0;
	}
}
	
static void RedrawAlarm()
{
	Check();
	char *at = malloc(sizeof(char) * 16);

	sprintf(at, "      %d%d:%d%d    ", alarmSetter[0], alarmSetter[1], alarmSetter[2], alarmSetter[3]);
		
	switch(alarmPosition)
	{
		case 0:
			SetScreenLine1("Alarm v        ");
			break;
			
		case 1:
			SetScreenLine1("Alarm  v       ");
			break;
			
		case 2:
			SetScreenLine1("Alarm    v     ");
			break;
			
		case 3:
			SetScreenLine1("Alarm     v    ");
			break;
			
		default:
			break;
	}
		
	SetScreenLine2(at);
	free(at);
}
	
void AlarmSettingLeft()
{
	if (alarmPosition < 0)
		alarmPosition = 0;
	else
		alarmPosition--;
		
	RedrawAlarm();
}

void AlarmSettingRight()
{
	if (alarmPosition > 3)
		alarmPosition = 3;
	else
		alarmPosition++;
		
	RedrawAlarm();
}

void AlarmSettingUp()
{
	alarmSetter[alarmPosition]++;
	RedrawAlarm();
}

void AlarmSettingDown()
{
	if (alarmSetter[alarmPosition] > 0)
		alarmSetter[alarmPosition]--;
		
	RedrawAlarm();
}

void AlarmSettingConfirm()
{
	int hh = Concatenate(alarmSetter[0], alarmSetter[1]);
	int mm = Concatenate(alarmSetter[2], alarmSetter[3]);
	
	Flash_WeekAlarm weekAlarms;
	ReadWeekAlarmFromFlash(&weekAlarms);
	
	Flash_Alarm flashTime = {hh, mm, 1};
	
	int i = 0;
	for(;i < 7; i++)
	{
		weekAlarms.alarms[i] = flashTime;
	}
	
	WriteWeekAlarmToFlash(weekAlarms);
	UpdateWeekAlarm(weekAlarms);
	DetermineNearestAlarm();
	
	ToAlarmMenu();
}
