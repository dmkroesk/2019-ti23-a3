/*
 * alarmmenu.c
 *
 * Created: 28-2-2019 15:55:19
 * Author: Ricky
 */ 

#define LOG_MODULE LOG_MAIN_MODULE

#include "draw.h"
#include "alarm.h"
#include "log.h"
#include "keylogic.h"
#include "radio.h"
#include "alarmmenu.h"


int alarmElement = 0;
int radioElement = 0;
char *alarmElements[5];
char *radiochannels[3];


void AlarmMenuLeft()
{
	alarmElement--;
	if(alarmElement < 0)
		alarmElement = 4;
	
	SetAlarmMenuElement();
}

void AlarmMenuRight()
{
	alarmElement++;
	if(alarmElement > 4)
		alarmElement = 0;
	
	SetAlarmMenuElement();
}


void InitAlarmMenuElements()
{
	alarmElements[0] = ("< Alle tijden >");
	alarmElements[1] = ("<   Re-Arm    >");
	alarmElements[2] = ("<   Geluid    >");
	alarmElements[3] = ("<   Radio     >");
	alarmElements[4] = ("< Weekalarmen >");
}

void SelectAlarmItem()
{
	switch (alarmElement)
	{
		case 0:
			ToAlarmTime();
			break;
		
		case 1:
			ToAlarmReArm();
			break;

		case 2:
			ToAlarmSound();
			break;
		
		case 3:
			ToAlarmRadio();
			break;
		
		case 4:
			ToWeekAlarm();
			break;
	}
}

void AlarmRadioMenuLeft()
{
	radioElement--;
	if(radioElement < 0)
		radioElement = 2;
	
	SetAlarmRadioMenuElement();
}

void AlarmRadioMenuRight()
{
	radioElement++;
	if(radioElement > 2)
		radioElement = 0;
	
	SetAlarmRadioMenuElement();
}

void InitAlarmRadioMenuElements()
{
	radiochannels[0] = ("<    Jazz     >");
	radiochannels[1] = ("<   C-Rock    >");
	radiochannels[2] = ("<  Aardschok  >");
}

void SelectRadioItem()
{
	SetRadioAlarm(radioElement);
	ToAlarmMenu();
}

void SetAlarmMenuElement()
{
	SetScreenLine2(alarmElements[alarmElement]);
}

void SetAlarmRadioMenuElement()
{
	SetScreenLine2(radiochannels[radioElement]);
}

void ReArmOnOff()
{
	if (GetReArm() == 1)
		SetReArmOff();	
	else
		SetReArmOn();
	
	SetReArmScreen();
}

void SetReArmScreen()
{
	if (GetReArm() == 1)
		SetScreenLine2("<      Aan    >");
	else
		SetScreenLine2("<      Uit    >");
}

void ItemConfirm()
{
	ToAlarmMenu();
}

void RadioOnOff()
{
	if (GetRadio() == 1)
		SetRadioOff();
	else
		SetRadioOn();
	
	SetSoundScreen();
}

void SetSoundScreen()
{
	 if (GetRadio() == 1)
		SetScreenLine2("<   Radio     >");
	 else
		SetScreenLine2("<   Beep      >"); 
}
