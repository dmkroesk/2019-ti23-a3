/*
 * states.c
 *
 * Created: 18/02/2019 20:05:53
 * Author: Ian
 */

#include "states.h"
#include "keybinding.h"
#include "draw.h"


int currentState;


void InitState()
{
	currentState = IMC_STATE_MAIN;
	SetButtons(currentState);
	InitScreen();
}

void SetState(int state)
{
	currentState = state;
	SetButtons(currentState);
}

int GetState()
{
	return currentState;
}
