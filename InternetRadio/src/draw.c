/*
* draw.c
*
* Created: 18-2-2019 20:08:05
* Author: Ian
*/

#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <avr/delay.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "display.h"
#include "remcon.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "uart0driver.h"
#include "mmc.h"
#include "watchdog.h"
#include "flash.h"
#include "spidrv.h"
#include "draw.h"
#include "rtc.h"
#include "x1205.h"
#include "keybinding.h"
#include "datetime.h"
#include "keylogic.h"
#include "alarm.h"
#include "storage.h"
#include "netcomponent.h"
#include "radio.h"
#include "states.h"


char screenLine1[DISPLAY_LENGTH];
char screenLine2[DISPLAY_LENGTH];


void InitScreen()
{
	UpdateDateTime();
}

void UpdateDateTime()
{
	tm * dateTime = NULL;
	GetDateTime(dateTime);
	
	char * time = (char*)malloc(13 * sizeof(char));
	sprintf(time, "     %02d:%02d     ",dateTime->tm_hour, dateTime->tm_min);
	
	if(GetPlaying() == 0)
	{
		char * date = (char*)malloc(13 * sizeof(char));
		sprintf(date, "   20%02d/%02d/%02d  ", dateTime->tm_year, dateTime->tm_mon + 1, dateTime->tm_mday);
		SetScreenLine2(date);
		free(date);
	}
	
	SetScreenLine1(time);
	
	free(time);
}

void DrawScreen()
{
	LcdWriteString(screenLine1, 0);
	LcdWriteString(screenLine2, 1);
	
	LcdSetCursor(0,0);
	
	if (GetSyncStatus() == 1 && GetState() == IMC_STATE_MAIN)
		LcdDrawSyncIcon();
	
	if (GetInternetStatus() == 1)
		LcdDrawInternetIcon();
	
	if (GetAlarmStatus() == 1)
		LcdDrawAlarmIcon();
}

void SetScreenLine1(char *line)
{
	int i = 0;
	for (; i < LINELENGTH; i++)
		screenLine1[i] = line[i];
}


void SetScreenLine2(char *line)
{
	int i = 0;
	for (; i < LINELENGTH; i++)
		screenLine2[i] = line[i];
}

char *GetScreenLine1()
{
	return screenLine1;
}

char *GetScreenLine2()
{
	return screenLine2;
}

void DrawVolume(int volume)
{
	int full = 0;
	int i = 0;
	SetScreenLine1("    Volume     ");
	char drawmid[] = " [          ]  ";
	
	if(volume == 0)
	{
		
	}else
	{
		full = volume/2;
		for(; i < full; i++)
		{
			drawmid[i+2] = '=';
		}
		if(volume%2 == 0 && volume == 0)
		{
			drawmid[0+2] = '-';
		}else if(volume%2 != 0)
		{
			drawmid[full+2] = '-';
		}
	}

	SetScreenLine2(drawmid);
	DrawScreen();
}

void DrawBass(int bass)
{
	int full = 0;
	int i = 0;
	char drawmid[] = "  [       ]    ";
	
	if(bass != 0)
	{
		full = bass/2;
		for(; i < full; i++)
		{
			drawmid[i+3] = '=';
		}
		if(bass%2 == 0 && bass == 0)
		{
			drawmid[0+3] = '-';
		}
		else if(bass%2 != 0)
		{
			drawmid[full+3] = '-';
		}
	}

	SetScreenLine2(drawmid);
	DrawScreen();
}

void DrawTreble(int treble)
{
	int full = 0;
	int i = 0;
	char drawmid[] = "   [   |   ]   ";
	
	if(treble != 0)
	{
		full = abs(treble)/2;
		if(treble > 0 )
		{
			for(; i < full; i++)
			{
				drawmid[i+8] = '=';
			}
			if(treble%2 == 0 && treble == 1)
			{
				drawmid[8] = '-';
			}
			else if(treble%2 != 0)
			{
				drawmid[full+8] = '-';
			}
		}
		else
		{
			for(i = 0; abs(i) < full; i--)
			{
				drawmid[i+6] = '=';
			}
			if(treble%2 == 0 && treble == 1)
			{
				drawmid[6] = '-';
			}
			else if(treble%2 != 0)
			{
				drawmid[i+6] = '-';
			}
		}
	}

	SetScreenLine2(drawmid);
	DrawScreen();
}
