/* 
 * weekalarms.c
 * 
 * Created: 19-3-2019 15:13:54
 * Author: Ian
 */

#include "weekalarms.h"
#include "states.h"
#include "weekday.h"


int currentWeekAlarm;
char *weekAlarms[7];


void InitWeekAlarmMenu()
{
	currentWeekAlarm = 0;
	
	SetWeekAlarmElement();
}

void InitWeekAlarmElements()
{
	weekAlarms[0] = ("< Maandag    > ");
	weekAlarms[1] = ("< Dinsdag    > ");
	weekAlarms[2] = ("< Woensdag   > ");
	weekAlarms[3] = ("< Donderdag  > ");
	weekAlarms[4] = ("< Vrijdag    > ");
	weekAlarms[5] = ("< Zaterdag   > ");
	weekAlarms[6] = ("< Zondag     > ");
}

void SetWeekAlarmElement()
{
	SetScreenLine2(weekAlarms[currentWeekAlarm]);
}

void WeekAlarm_Left(void)
{
	currentWeekAlarm --;
	if(currentWeekAlarm < 0)
		currentWeekAlarm = (sizeof(weekAlarms)/sizeof(char*) - 1);
	
	SetWeekAlarmElement();
}

void WeekAlarm_Right(void)
{
	currentWeekAlarm ++;
	if(currentWeekAlarm > (sizeof(weekAlarms)/sizeof(char*) - 1))
		currentWeekAlarm = 0;
	
	SetWeekAlarmElement();
}

void WeekAlarm_SelectItem()
{
	SetState(IMC_STATE_MENU_ALARM_WEEK_DAY);
	InitWeekDay(currentWeekAlarm);
}
