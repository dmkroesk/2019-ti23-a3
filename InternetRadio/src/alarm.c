/*
* alarm.c
*
* Created: 21/02/2019 11:11:10
* Author: Boudewijn
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include <time.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "alarm.h"
#include "draw.h"
#include "states.h"
#include "vs10xx.h"
#include "datetime.h"
#include "rtc.h"
#include "log.h"
#include "display.h"
#include "soundcontroller.h"
#include "storage.h"
#include "netcomponent.h"
#include "streamplayer.h"
#include "keylogic.h"
#include "radio.h"


volatile Alarm currentAlarm;
Flash_WeekAlarm fWeekAlarms;
volatile int alarmStatus;
volatile int rangSeconds;
int alarmOn = 0;


/*
	Creates a thread to check if the alarm is occurred. This needs to be changed so it polls with the method from the RTC chip.

	@param alarm_thread - Name of the thread.
	@param arg - Arguments.
*/
THREAD(alarm_thread, args)
{
	NutThreadSetPriority(32);
	tm *currentDateTime = NULL;
	
	for (;;)
	{
		GetDateTime(currentDateTime);
		
		// Resets alarm when went off.
		if (currentDateTime->tm_min != currentAlarm.alarmTime.MN && alarmStatus == 0 && GetReArm() == 1)
		{
			DetermineNearestAlarm();
			AlarmCheckOn();
			currentAlarm.snooze = 0;
			DrawScreen();
		}

		if (currentDateTime->tm_hour == currentAlarm.alarmTime.HR && currentDateTime->tm_min == currentAlarm.alarmTime.MN && alarmStatus == 1 && GetState() != IMC_STATE_ALARM)
			Ring();
		
		NutSleep(1000);
	}
	
	NutThreadExit();
	NutThreadDestroy();
}

THREAD(snooze_thread, args)
{
	NutSleep(10000);
	
	Ring();
	currentAlarm.snooze = 1;
	
	NutSleep(500);
	NutThreadExit();
	NutThreadDestroy();
}

THREAD(waitMinute_thread, args)
{
	NutThreadSetPriority(32);
	tm *datetime = NULL;

	for (;;)
	{
		GetDateTime(datetime);

		if (CheckAutoAlarmDone(datetime) == 1)
		{
			AlarmDone();
			NutThreadExit();
			NutThreadDestroy();
		}

		if (GetState() == IMC_STATE_MAIN)
		{
			NutThreadExit();
			NutThreadDestroy();
		}

		NutSleep(1000);
	}

	NutThreadExit();
	NutThreadDestroy();
}

int CheckAutoAlarmDone(tm *currentTime)
{
	int min = currentAlarm.alarmTime.MN + 1;
	int hr = currentAlarm.alarmTime.HR;
	int sc = GetDateTimeSeconds();

	if (min > 59)
	{
		min -= 60;
		hr++;
		if (hr > 23)
			hr -= 24;
	}

	if (currentTime->tm_hour == hr && currentTime->tm_min == min && sc >= rangSeconds)
		return 1;

	return 0;
}

void AlarmInit()
{
	ReadWeekAlarmFromFlash(&fWeekAlarms);
	
	DetermineNearestAlarm();
	SetReArmOff();
	NutThreadCreate("alarm_thread", alarm_thread, NULL, 256);
}

void Re_Arm()
{
	currentAlarm.alarmTime.DT++;
}

void AlarmOff()
{
	Re_Arm();
	VsBeepStop();
	alarmStatus = 0;
	currentAlarm.status = alarmStatus;
	SetAlarmOff();
	StopStream();
}

void SetAlarmTime(int HR, int MN)
{
	currentAlarm.alarmTime.HR = HR;
	currentAlarm.alarmTime.MN = MN;
	Flash_Alarm fAlarm = {HR, MN, alarmStatus};
	WriteAlarmToFlash(fAlarm);
}

void Ring()
{
	StopStream();
	NutSleep(200);
	
	alarmStatus = 1;
	alarmOn = 1;
	SetState(IMC_STATE_ALARM);
	
	SetScreenLine1("     Wakker    ");
	SetScreenLine2("     Worden!   ");
	
	if(GetPlaying() == 0 && GetInternetStatus() == 1 && GetRadio() == 1)
		StartAlarmRadio();
	else
		PlayTone();
	
	currentAlarm.snooze = 0;
	
	rangSeconds = GetDateTimeSeconds();
	if (rangSeconds >= 58)
		rangSeconds = 0;
	
	NutThreadCreate("waitMinute_thread", waitMinute_thread, NULL, 256);
}

void Snooze()
{
	alarmStatus = 2;
	NutThreadCreate("snooze_thread", snooze_thread, NULL, 256);
}

void GetAlarmTime(tm *tm)
{
	X12RtcGetAlarm(0, tm, 0b00000110);
}

void OnOffSwitch()
{
	int dow = GetDOW();
	if (alarmStatus == 0)
	{
		Flash_Alarm selectedAlarm = {fWeekAlarms.alarms[dow].HH,fWeekAlarms.alarms[dow].MN, 1};
		fWeekAlarms.alarms[dow] = selectedAlarm;
		alarmStatus = 1;
	}
	else
	{
		Flash_Alarm selectedAlarm = {fWeekAlarms.alarms[dow].HH,fWeekAlarms.alarms[dow].MN, 0};
		fWeekAlarms.alarms[dow] = selectedAlarm;
		alarmStatus = 0;
		SetReArmOff();
	}
	DrawScreen();
	
	WriteWeekAlarmToFlash(fWeekAlarms);
	DetermineNearestAlarm();
}

void AlarmCheckOn()
{
	alarmStatus = 1;
	currentAlarm.status = alarmStatus;
}

int GetAlarmStatus()
{
	return alarmStatus;
}

int GetReArm()
{
	return currentAlarm.ReArm;
}

void SetReArmOff()
{
	currentAlarm.ReArm = 0;
}

void SetReArmOn()
{
	currentAlarm.ReArm = 1;
}
int GetRadio()
{
	return currentAlarm.Radio;
}

void SetRadioOff()
{
	currentAlarm.Radio = 0;
}

void SetRadioOn()
{
	currentAlarm.Radio = 1;
}

int GetAlarmOn()
{
	return alarmOn;
}

void SetAlarmOff()
{
	alarmOn = 0;
}

void DetermineNearestAlarm()
{
	int day = GetDOW();
	
	int result = -1;
	
	result = IsAlarmAfterCurrentTime(day);
	
	//If no alarm is active, set status to off and default the alarm to -1
	if(result == -1)
	{
		alarmStatus = 0;
		SetAlarmTime(-1, -1);
	}
	//Otherwise, set alarm time
	else
	{
		alarmStatus = 1;
		SetAlarmTime(fWeekAlarms.alarms[day].HH, fWeekAlarms.alarms[day].MN);	
	}	
}

/*
	Checks if the currently selected alarm occurs after the current time
	Returns 0 if it is after the current time, and -1 otherwise
	
	@param day - The current day
	
*/
int IsAlarmAfterCurrentTime(int day){
	struct _tm datetime;
	GetDateTime(&datetime);
	
	//If the alarm is off, return -1
	if(fWeekAlarms.alarms[day].alarmStatus == 0)
		return -1;
			
	int sumCurrentTime = 0;	
	sumCurrentTime += datetime.tm_min;
	sumCurrentTime += datetime.tm_hour * 100;
	
	int sumAlarmTime = 0;
	sumAlarmTime += fWeekAlarms.alarms[day].MN;
	sumAlarmTime += fWeekAlarms.alarms[day].HH * 100;	
	
	int returnValue = -1;
	
	if(sumAlarmTime >= sumCurrentTime)
		returnValue = 0;
		
	return returnValue; 
}

void UpdateWeekAlarm(Flash_WeekAlarm weekAlarm){
	fWeekAlarms = weekAlarm;
}
