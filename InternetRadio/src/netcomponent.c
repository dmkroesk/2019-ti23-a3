/*
* netcomponent.c
*
* Created: 26/02/2019 14:49:57
* Author: Boudewijn & Ruben
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include <stdio.h>
#include <string.h>

#include "log.h"
#include "netcomponent.h"
#include "radio.h"
#include "storage.h"
#include "networkmenu.h"
#include "datetime.h"


static void ApplyTimeZone(tm *);
static void ApplyTimezoneTimeChange(int *, int *);


static char eth0IfName[9] = "eth0";
int internetState = 0;

FILE *stream;
TCPSOCKET *stream_sock;


/*
	[THREAD]
	Initializes the network configuration including registering a device & calling DHCP.
	Then calls the callback set in StartInitNetwork.
*/
THREAD(init_network, args)
{
	unsigned int newmac[6];
	GetMACAddress(&newmac);
	
	uint8_t mac_addr[6] = {newmac[0], newmac[1], newmac[2], newmac[3], newmac[4], newmac[5]};
	int result = OK;
	
	// Registreer NIC device (located in nictrl.h)
	result = NutRegisterDevice(&DEV_ETHER, ETH0_BASE, ETH0_IRQ);
	if (result)
		result = NOK;

	if (OK == result)
		if (NutDhcpIfConfig(eth0IfName, mac_addr, 0))
			result = NOK;

	if (OK == result)
	{
		RequestConfig();
		internetState = 1;
		if (GetSyncStatus() == 1)
			SyncTime();
	}
	else
	{
		internetState = 0;
	}

	NutSleep(500);

	NutThreadExit();
	NutThreadDestroy();
}

int SyncTime()
{
	return GetNtpTime("129.250.35.250");
}

int GetNtpTime(char *address)
{
	tm time;
	X12RtcGetClock(&time);
	
	u_long addr = inet_addr(address);
	time_t tmpTime = 0;
	int i = NutSNTPGetTime(&addr, &tmpTime);
	gmtime_r(&tmpTime, &time);
	
	ApplyTimeZone(&time);
	
	if (time.tm_hour < 0)
		SetPreviousDay(time.tm_wday, time.tm_year, time.tm_mon, time.tm_mday, time.tm_hour, time.tm_min);
	else
		X12RtcSetClock(&time);
	
	return i;
}

static void ApplyTimeZone(tm *time)
{
	int hr = time->tm_hour;
	int min = time->tm_min;
	
	ApplyTimezoneTimeChange(&hr, &min);
	
	time->tm_hour = hr;
	time->tm_min = min;
}

static void ApplyTimezoneTimeChange(int *hr, int *min)
{
	Flash_TimeSync timezone;
	ReadTimeSyncFromFlash(&timezone);
	
	switch (timezone.timezone)
	{
		case UTC_MIN_12:
			*hr -= 12;
			break;
		case UTC_MIN_11:
			*hr -= 11;
			break;
		case UTC_MIN_10:
			*hr -= 10;
			break;
		case UTC_MIN_9_30:
			*hr -= 9;
			*min -= 30;
			break;
		case UTC_MIN_9:
			*hr -= 9;
			break;
		case UTC_MIN_8:
			*hr -= 8;
			break;
		case UTC_MIN_7:
			*hr -= 7;
			break;
		case UTC_MIN_6:
			*hr -= 6;
			break;
		case UTC_MIN_5:
			*hr -= 5;
			break;
		case UTC_MIN_4:
			*hr -= 4;
			break;
		case UTC_MIN_3_30:
			*hr -= 3;
			*min -= 30;
			break;
		case UTC_MIN_3:
			*hr -= 3;
			break;
		case UTC_MIN_2_30:
			*hr -= 2;
			*min -= 30;
			break;
		case UTC_MIN_2:
			*hr -= 2;
			break;
		case UTC_MIN_1:
			*hr -= 1;
			break;
		case UTC:
			break;
		case UTC_PLUS_1:
			*hr += 1;
			break;
		case UTC_PLUS_2:
			*hr += 2;
			break;
		case UTC_PLUS_3:
			*hr += 3;
			break;
		case UTC_PLUS_3_30:
			*hr += 3;
			*min += 30;
			break;
		case UTC_PLUS_4:
			*hr += 4;
			break;
		case UTC_PLUS_4_30:
			*hr += 4;
			*min += 30;
			break;
		case UTC_PLUS_5:
			*hr += 5;
			break;
		case UTC_PLUS_5_30:
			*hr += 5;
			*min += 30;
			break;
		case UTC_PLUS_5_45:
			*hr += 5;
			*min += 45;
			break;
		case UTC_PLUS_6:
			*hr += 6;
			break;
		case UTC_PLUS_6_30:
			*hr += 6;
			*min += 30;
			break;
		case UTC_PLUS_7:
			*hr += 7;
			break;
		case UTC_PLUS_8:
			*hr += 8;
			break;
		case UTC_PLUS_8_45:
			*hr += 8;
			*min += 45;
			break;
		case UTC_PLUS_9:
			*hr += 9;
			break;
		case UTC_PLUS_9_30:
			*hr += 9;
			*min += 30;
			break;
		case UTC_PLUS_10:
			*hr += 10;
			break;
		case UTC_PLUS_10_30:
			*hr += 10;
			*min += 30;
			break;
		case UTC_PLUS_11:
			*hr += 11;
			break;
		case UTC_PLUS_12:
			*hr += 12;
			break;
		case UTC_PLUS_12_45:
			*hr += 12;
			*min += 45;
			break;
		case UTC_PLUS_13:
			*hr += 13;
			break;
		case UTC_PLUS_13_45:
			*hr += 13;
			*min += 45;
			break;
		case UTC_PLUS_14:
			*hr += 14;
			break;
	}
	
	if (*min > 59)
	{
		*min -= 60;
		*hr++;
	}
	else if (*min < 0)
	{
		*min += 60;
		*hr--;
	}
}

int IsDST(int DT, int MO, int DW)
{
	if (MO < 3 || MO > 11) // if January, February or December
		return 0;
	
	if (MO > 3 && MO < 11) // if in between April and October
		return 1;
	
	int previousSunday = DT - DW;
	if (MO == 3) // if previous sunday in march was on or after the 8th
		return previousSunday >= 8;
	
	return previousSunday <= 0; // if previous sunday in november was before the 1st
}

void StartInitNetwork(void (*callback)(int))
{
	NutThreadCreate("network_init_thread", init_network, NULL, 768);
}

TCPSOCKET *Connect(CONST char *addr, int port, char *route)
{
	uint32_t u_address = inet_addr(addr);
	uint16_t u_port = (uint16_t)port;
	
	TCPSOCKET *sock;
	sock = NutTcpCreateSocket();
	
	if (NutTcpConnect(sock, u_address, u_port))
	{
		internetState = 0;
		SetPlaying(0);
	}
	else
		internetState = 1;

	return sock;
}

int OpenStream(RadioChannel channel, char *route)
{
	int result = OK;
	char *data;

	uint32_t u_address = inet_addr(channel.url);

	stream_sock = Connect(channel.url, channel.port, route);
	stream = _fdopen((int)stream_sock, "r+b");

	fprintf(stream, "GET %s HTTP/1.0\r\n", route);
	fprintf(stream, "Host: %s\r\n", u_address);
	fprintf(stream, "User-Agent: Ethernut\r\n");
	fprintf(stream, "Accept: */*\r\n");
	fprintf(stream, "Icy-MetaData: 0\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);

	// Server stuurt nu HTTP header terug, catch in buffer
	data = (char *)malloc(512 * sizeof(char));

	while (fgets(data, 512, stream))
		if (0 == *data)
			break;
	
	free(data);

	return result;
}

int GetInternetStatus()
{
	return internetState;
}

FILE *GetStream()
{
	return stream;
}
