/*! \mainpage SIR firmware documentation
*
*  \section intro Introduction
*  A collection of HTML-files has been generated using the documentation in the sourcefiles to
*  allow the developer to browse through the technical documentation of this project.
*  \par
*  \note these HTML files are automatically generated (using DoxyGen) and all modifications in the
*  documentation should be done via the sourcefiles.
*/

/*! \file
*  COPYRIGHT (C) STREAMIT BV 2010
*  \date 19 december 2003
*/

#define LOG_MODULE LOG_MAIN_MODULE


/*--------------------------------------------------------------------------*/
/*  Include files                                                           */
/*--------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include <avr/delay.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "display.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "flash.h"
#include "rtc.h"
#include "datetime.h"
#include "watchdog.h"
#include "x1205.h"
#include "remcon.h"
#include "uart0driver.h"
#include "mmc.h"
#include "spidrv.h"
#include "draw.h"
#include "states.h"
#include "alarm.h"
#include "netcomponent.h"
#include "soundcontroller.h"
#include "storage.h"
#include "radio.h"
#include "keybinding.h"


static volatile int bl_on = 0;


/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/


/*
	The backlight thread is called when the backlight should turn on.
	if bl_on is set outside the thread, then backlight will turn off.
	else then backlight will turn of after 10 seconds.
	
	@param backlight_thread - Name of the thread.
	@param arg - Arguments.
*/
THREAD(backlight_thread, args)
{
	int t = 0;
	while(1)
	{
		if(bl_on == 1 || GetAlarmOn() == 1)
		{
			t = 0;
			bl_on = 0;
		}
			
		if(t < 100)
		{
			t += 1;
			LcdBackLight(LCD_BACKLIGHT_ON);
		}
		else
			LcdBackLight(LCD_BACKLIGHT_OFF);
			
		NutSleep(100);
	}
	
	NutThreadExit();
	NutThreadDestroy();
}

/*
	Thread that keeps checking if the time in the RTC has changed.
	When the time has changed the time will be updated.
	
	@param timeChangedDetection - Name of the thread.
	@param arg - Arguments.
*/
THREAD(timeChangedDetection, args)
{
	NutThreadSetPriority(50);

	tm *dateTime = NULL;
	GetDateTime(dateTime);
	int minutes = dateTime->tm_min;

	for (;;)
	{
		if (GetState() == IMC_STATE_MAIN)
		{
			GetDateTime(dateTime);
				
			Flash_TimeSync timeSync;
			ReadTimeSyncFromFlash(&timeSync);
			if (IsDST(dateTime->tm_mday, dateTime->tm_mon, dateTime->tm_wday) == 1 && timeSync.isDST == 0)
			{
				dateTime->tm_hour = dateTime->tm_hour + 1;
					
				timeSync.isDST = 1;
					
				WriteTimeSyncToFlash(timeSync);
			}
			else if (IsDST(dateTime->tm_mday, dateTime->tm_mon, dateTime->tm_wday) == 0 && timeSync.isDST == 1)
			{
				dateTime->tm_hour = dateTime->tm_hour - 1;
					
				timeSync.isDST = 0;
					
				WriteTimeSyncToFlash(timeSync);
			}
				
			if (dateTime->tm_hour > 23)
				SetNextDay(dateTime->tm_wday, dateTime->tm_year, dateTime->tm_mon + 1, dateTime->tm_mday, dateTime->tm_hour, dateTime->tm_min);
			else if (dateTime->tm_hour < 0)
				SetPreviousDay(dateTime->tm_wday, dateTime->tm_year, dateTime->tm_mon + 1, dateTime->tm_mday, dateTime->tm_hour, dateTime->tm_min);

			if (minutes != dateTime->tm_min)
			{
				UpdateDateTime();
				minutes = dateTime->tm_min;
				//RequestConfig();
			}

			if (dateTime->tm_min == 0)
			{
				if (GetInternetStatus() == 1 && GetSyncStatus() == 1)
					SyncTime();
			}
		}
		// Wait for 1 sec
		NutSleep(1000);
	}
	
	// Exit and destroy thread when done.
	NutThreadExit();
	NutThreadDestroy();
	for (;;)
	{
		NutSleep(10000);
	}
}

/*
	Continuous loop that runs in a thread looking for key presses.
	After a key-press was found it turns the LCD backlight on and flashes the LED indicating a key was pressed.
	The key will be then be decoded to a function.
	if the function was NULL nothing happens else the function will be executed.
	Lastly it redraws the screen.
	
	@param keyDetectionThread - Name of the thread.
	@param arg - Arguments.
*/
THREAD(keyDetectionThread, args)
{
	NutThreadSetPriority(32);

	u_char key;
	void (*func)();
	u_char event;
	for (;;)
	{
		event = KbWaitForKeyEvent(50);

		key = kbGetRemappedKey();
		NutSleep(100);

		func = GetValue(key);
		if (key != 136)
		{
			int i = 0;
			for (; i < 4; i++)
			{
				LedControl(LED_TOGGLE);
				NutSleep(50);
			}
			bl_on = 1;
		}
		// Check if the input was valid
		if (func != NULL)
		{
			// If input was valid, execute the function
			func();
			func = NULL;
		}

		while (key == kbGetRemappedKey() && key != 136)
			NutSleep(100);

		// Redraw screen
		DrawScreen();

		// Wait 1 ms to not overflow mcu
		NutSleep(1);
	}

	// Exit and destroy thread when done.
	NutThreadExit();
	NutThreadDestroy();
	for (;;)
	{
		NutSleep(10000);
	}
}

/*
	Library function to wait for more than the maximum of 30 ms.

	@param ms - Time to wait in milliseconds.
*/
void wait(int ms)
{
	int t = 0;
	for (; t < ms; t++)
		_delay_ms(1);
}

/*
	This function is used as a callback for StartInitNetwork. It's called when the device has a network connection.
	
	@param success - This indicates whether the connection was successfully made.
*/
void OnInitDone(int success)
{
	printf("In OnInitDone success = %d\n", success);
	// TODO: Set Network Icon according to success
}

/* ����������������������������������������������������������������������� */
/*!
* \brief Main entry of the SIR firmware
*
* All the initialisations before entering the for(;;) loop are done BEFORE
* the first key is ever pressed. So when entering the Setup (POWER + VOLMIN) some
* initialisatons need to be done again when leaving the Setup because new values
* might be current now
*
* \return \b never returns
*/
/* ����������������������������������������������������������������������� */
int main(void)
{
	bl_on = 0;
	NutThreadCreate("background_thread", backlight_thread, NULL, 256);
	
	WatchDogDisable();
	
	NutDelay(100);
	
	SysInitIO();
	SPIinit();
	LedInit();
	LcdLowLevelInit();
	Uart0DriverInit();
	Uart0DriverStart();
	LogInit();
	CardInit();
	X12Init();
	x1205Init();
	
	NutSleep(100);
	
	x1205Enable();
	RcInit();
	KbInit();
	SysControlMainBeat(ON); // enable 4.4 msecs hartbeat interrupt
	
	NutThreadSetPriority(1);
	
	/* Enable global interrupts */
	sei();
	
	StartInitNetwork(OnInitDone);
	
	InitRadioList(); 
	InitFlashStorage();
	AlarmInit();
	RcInit();
	InitScreen();
	InitStream();
	InitButtons();
	InitState();
	LcdBackLight(LCD_BACKLIGHT_ON);
	
	NutThreadCreate("keyDetectionThread", keyDetectionThread, NULL, 256);
	NutThreadCreate("timeChangedDetection", timeChangedDetection, NULL, 256);
	
	for (;;)
	{
		if (bitsReady() == 1)
		{
			StartRemoteFunction(getBits());
			ResetBits();
			bl_on = 1;
		}
		
		NutSleep(100);
		WatchDogRestart();
	}
	
	return (0); // never reached, but 'main()' returns a non-void, so.....
}
