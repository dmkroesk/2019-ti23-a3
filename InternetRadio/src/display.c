/* ========================================================================
* [PROJECT]    SIR100
* [MODULE]     Display
* [TITLE]      display source file
* [FILE]       display.c
* [VSN]        1.0
* [CREATED]    26092003
* [LASTCHNGD]  06102006
* [COPYRIGHT]  Copyright (C) STREAMIT BV
* [PURPOSE]    contains all interface- and low-level routines to
*              control the LCD and write characters or strings (menu-items)
* ======================================================================== */

#define LOG_MODULE LOG_DISPLAY_MODULE
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/timer.h>
#include <sys/event.h>
#include <sys/thread.h>
#include <sys/heap.h>

#include "system.h"
#include "portio.h"
#include "display.h"
#include "log.h"


/*
	Writes one byte to the LCD-controller (by  calling LcdWriteNibble twice)
		CtrlState determines if the byte is written to the instruction register
		or to the data register.
	
	@param CtrlState - Instruction or data.
	@param LcdByte - Byte to write.
*/
static void LcdWriteByte(u_char, u_char);

/*
	Writes a nibble to the LCD-controller (interface is a 4-bit databus, so
		only 4 databits can be send at once).
		The nibble to write is in the upper 4 bits of LcdNibble.
	
	@param CtrlState - Instruction or data.
	@param LcdNibble - Nibble to write (upper 4 bits in this byte).
*/
static void LcdWriteNibble(u_char, u_char);

/*
	This routine repeatetly reads the databus and checks if the highest bit (bit 7)
		has become '0'. If a '0' is detected on bit 7 the function returns.
*/
static void LcdWaitBusy(void);

/*
	Draws the internet and alarm icons into the ram on slot 0 and 1.
*/
static void LcdWriteIconsToRam(void);


static unsigned char internet[] = {
	0x0a, 0x0a, 0x1f, 0x1f, 0x0e, 0x04, 0x04, 0x04
};

static unsigned char alarm[] = {
	0x00, 0x00, 0x0e, 0x15, 0x17, 0x11, 0x0e, 0x00
};

static unsigned char sync[] = {
	0x08, 0x1C, 0x08, 0x0A, 0x0A, 0x02, 0x07, 0x02
};

static char text[DISPLAY_LENGTH] = {"                                                                                "};
static u_char bl_state = LCD_BACKLIGHT_STATE_OFF;


void LcdBackLight(u_char Mode)
{
	if (Mode == LCD_BACKLIGHT_ON)
	{
		sbi(LCD_BL_PORT, LCD_BL_BIT); // Turn on backlight
		bl_state = LCD_BACKLIGHT_STATE_ON;
	}

	if (Mode == LCD_BACKLIGHT_OFF)
	{
		cbi(LCD_BL_PORT, LCD_BL_BIT); // Turn off backlight
		bl_state = LCD_BACKLIGHT_STATE_OFF;
	}
}

void LcdChar(char MyChar)
{
	LcdWriteByte(WRITE_DATA, MyChar);
}

void LcdLowLevelInit()
{
	u_char i;
		
	NutDelay(140);                              // wait for more than 140 ms after Vdd rises to 2.7 V
		
	for (i=0; i<3; ++i)
	{
		LcdWriteNibble(WRITE_COMMAND, 0x33);    // function set: 8-bit mode; necessary to guarantee that
		NutDelay(4);                            // SIR starts up always in 5x10 dot mode
	}

	LcdWriteNibble(WRITE_COMMAND, 0x22);		// function set: 4-bit mode; necessary because KS0070 doesn't
	NutDelay(1);								// accept combined 4-bit mode & 5x10 dot mode programming

	//LcdWriteByte(WRITE_COMMAND, 0x24);        // function set: 4-bit mode, 5x10 dot mode, 1-line
	LcdWriteByte(WRITE_COMMAND, 0x28);			// function set: 4-bit mode, 5x7 dot mode, 2-lines
	NutDelay(5);

	LcdWriteByte(WRITE_COMMAND, 0x0C);			// display ON/OFF: display ON, cursor OFF, blink OFF
	NutDelay(5);

	LcdWriteByte(WRITE_COMMAND, 0x01);			// display clear
	NutDelay(5);
	
	LcdWriteByte(WRITE_COMMAND, 0x06);			// entry mode set: increment mode, intire shift off

	LcdWriteByte(WRITE_COMMAND, 0x80);          // DD-RAM address counter (cursor pos) to '0'
	
	LcdWriteIconsToRam();						// Draws the internet and alarm icons in the ram for later use
}

static void LcdWriteByte(u_char CtrlState, u_char LcdByte)
{
	LcdWaitBusy();								// see if the controller is ready to receive next byte
	LcdWriteNibble(CtrlState, LcdByte & 0xF0);
	LcdWriteNibble(CtrlState, LcdByte << 4);
}

static void LcdWriteNibble(u_char CtrlState, u_char LcdNibble)
{
	outp((inp(LCD_DATA_DDR) & 0x0F) | 0xF0, LCD_DATA_DDR); // set data-port to output again

	outp((inp(LCD_DATA_PORT) & 0x0F) | (LcdNibble & 0xF0), LCD_DATA_PORT); // prepare databus with nibble to write

	if (CtrlState == WRITE_COMMAND)
		cbi(LCD_RS_PORT, LCD_RS); // command: RS low
	else
		sbi(LCD_RS_PORT, LCD_RS); // data: RS high

	sbi(LCD_EN_PORT, LCD_EN);

	asm("nop\n\tnop"); // small delay

	cbi(LCD_EN_PORT, LCD_EN);
	cbi(LCD_RS_PORT, LCD_RS);
	outp((inp(LCD_DATA_DDR) & 0x0F), LCD_DATA_DDR);			 // set upper 4-bits of data-port to input
	outp((inp(LCD_DATA_PORT) & 0x0F) | 0xF0, LCD_DATA_PORT); // enable pull-ups in data-port
}

static void LcdWaitBusy()
{
	u_char Busy = 1;
	u_char LcdStatus = 0;

	cbi(LCD_RS_PORT, LCD_RS); // select instruction register

	sbi(LCD_RW_PORT, LCD_RW); // we are going to read

	while (Busy)
	{
		sbi(LCD_EN_PORT, LCD_EN); // set 'enable' to catch 'Ready'

		asm("nop\n\tnop");			  // small delay
		LcdStatus = inp(LCD_IN_PORT); // LcdStatus is used elsewhere in this module as well
		Busy = LcdStatus & 0x80;	  // break out of while-loop cause we are ready (b7='0')
	}

	cbi(LCD_EN_PORT, LCD_EN); // all ctrlpins low
	cbi(LCD_RS_PORT, LCD_RS);
	cbi(LCD_RW_PORT, LCD_RW); // we are going to write
}

static void LcdWriteIconsToRam()
{
	LcdWriteByte(WRITE_COMMAND, 0x40);
	
	int i;
	for (i = 0; i < 8; i++)
		LcdWriteByte(WRITE_DATA, internet[i]);
	
	LcdWriteByte(WRITE_COMMAND, 0x48);
	
	for (i = 0; i < 8; i++)
		LcdWriteByte(WRITE_DATA, alarm[i]);
	
	LcdWriteByte(WRITE_COMMAND, 0x56);
	
	for (i = 0; i < 8; i++)
		LcdWriteByte(WRITE_DATA, sync[i]);
	
	LcdWriteByte(WRITE_COMMAND, 0x80);
}

void LcdDrawInternetIcon()
{
	LcdSetCursor(0, 15);
	LcdWriteByte(WRITE_DATA, 0x00);
	LcdWriteByte(WRITE_COMMAND, 0x80);
}

void LcdDrawAlarmIcon()
{
	LcdSetCursor(1, 15);
	LcdWriteByte(WRITE_DATA, 0x01);
	LcdWriteByte(WRITE_COMMAND, 0x80);
}

void LcdDrawSyncIcon()
{
	LcdSetCursor(0, 0);
	LcdWriteByte(WRITE_DATA, 0x02);
	LcdWriteByte(WRITE_COMMAND, 0x80);
}

void LcdWriteString(char *s, int line)
{
	int i = 0;
	size_t len = strlen(s);
	int pos;
	
	if (line == 0)
		pos = 0;
	else
		pos = 40;

	for (; i < len; i++)
		text[i + pos] = s[i];
	
	for (i = 0; i < DISPLAY_LENGTH; i++)
		LcdChar(text[i]);
}

void LcdClear()
{
	LcdWriteByte(WRITE_COMMAND, 0x01);          // display clear
	LcdWriteByte(WRITE_COMMAND, 0x80);
}
	
void LcdShiftCursor(int direction, int amount)
{
	int i = 0;
	if (direction==LCD_CURSOR_LEFT)
		for (; i < amount; i++)
			LcdWriteByte(WRITE_COMMAND, 0b0000010000);
}

void LcdShiftDisplay(int direction, int amount)
{
	int i = 0;
	if (direction == LCD_CURSOR_LEFT)
		for (; i < amount; i++)
			LcdWriteByte(WRITE_COMMAND, 0b0000011000);
}

void LcdSetCursor(int line, int position)
{
	if (line == LINE_0)
		LcdWriteByte(WRITE_COMMAND, 0x80 + position);
	else if (line == LINE_1)
		LcdWriteByte(WRITE_COMMAND, 0xC0 + position);
}

void LcdSetCursorBlinkState(int state)
{
	if (state == LCD_CURSOR_BLINK_ON)
		LcdWriteByte(WRITE_COMMAND, 0x0E); // display ON, cursor ON, blink ON
	else
		LcdWriteByte(WRITE_COMMAND, 0x0C); // display ON, cursor OFF, blink OFF
}
