/*
 * storage.c
 *
 * Created: 26-2-2019 15:07:31
 * Author: Ian
 */

#define PAGE_ALARM 0x02
#define PAGE_PERIODIC_ALARM 0x03
#define PAGE_TESTMEMORY 0x04
#define PAGE_TIMEZONE 0x05
#define PAGE_MAC 0x06
#define PAGE_VOLUME 0x08
#define PAGE_WEEKALARM 0x08
#define PAGE_TREBLE 0x09
#define PAGE_BASS 0x10

#include "storage.h"
#include "flash.h"


void InitFlashStorage(void)
{
	At45dbInit();
}

void ResetFlashStorage(void)
{	
	Flash_MAC mac;
	At45dbPageWrite(PAGE_MAC, &mac, sizeof(Flash_MAC));
	
	Flash_Alarm alarm;
	At45dbPageWrite(PAGE_ALARM, &alarm, sizeof(Flash_Alarm));
	
	Flash_TimeSync timezone;
	At45dbPageWrite(PAGE_TIMEZONE, &timezone, sizeof(Flash_TimeSync));
}

void WriteMACToFlash(Flash_MAC mac)
{	
	At45dbPageWrite(PAGE_MAC, &mac, sizeof(Flash_MAC));
}

void ReadMACFromFlash(Flash_MAC *mac){
	At45dbPageRead(PAGE_MAC, mac, sizeof(Flash_MAC));
}

void WriteAlarmToFlash(Flash_Alarm alarm)
{
	At45dbPageWrite(PAGE_ALARM, &alarm, sizeof(Flash_Alarm));
}

void ReadAlarmFromFlash(Flash_Alarm *alarm)
{
	At45dbPageRead(PAGE_ALARM, alarm, sizeof(Flash_Alarm));
}

void WriteTimeSyncToFlash(Flash_TimeSync timezone)
{
	At45dbPageWrite(PAGE_TIMEZONE, &timezone, sizeof(Flash_TimeSync));
}

void ReadTimeSyncFromFlash(Flash_TimeSync *timezone)
{
	At45dbPageRead(PAGE_TIMEZONE, timezone, sizeof(Flash_TimeSync));
}

void WriteWeekAlarmToFlash(Flash_WeekAlarm weekAlarm)
{
	At45dbPageWrite(PAGE_WEEKALARM, &weekAlarm, sizeof(Flash_WeekAlarm));
}

void ReadWeekAlarmFromFlash(Flash_WeekAlarm *weekAlarm)
{
	At45dbPageRead(PAGE_WEEKALARM, weekAlarm, sizeof(Flash_WeekAlarm));
}

void WriteVolumeToFlash(Flash_Volume volume)
{
	At45dbPageWrite(PAGE_ALARM, &volume, sizeof(Flash_Volume));
}

void ReadVolumeFromFlash(Flash_Volume *volume)
{
	At45dbPageRead(PAGE_ALARM, volume, sizeof(Flash_Volume));
}

void WriteBassToFlash(Flash_Bass bass)
{
	At45dbPageWrite(PAGE_BASS, &bass, sizeof(Flash_Bass));
}

void ReadBassFromFlash(Flash_Bass *bass)
{
	At45dbPageRead(PAGE_BASS, bass, sizeof(Flash_Bass));
}

void WriteTrebleToFlash(Flash_Treble treble)
{
	At45dbPageWrite(PAGE_TREBLE, &treble, sizeof(Flash_Treble));
}

void ReadTrebleFromFlash(Flash_Treble *treble)
{
	At45dbPageRead(PAGE_TREBLE, treble, sizeof(Flash_Treble));
}

int GetSyncStatus()
{
	Flash_TimeSync timeSync;
	ReadTimeSyncFromFlash(&timeSync);
	
	return timeSync.synched;
}

char *TestStorage(void)
{
	Flash_Alarm alarm = {15,28};
	WriteAlarmToFlash(alarm);
	
	Flash_Alarm newAlarm;
	ReadAlarmFromFlash(&newAlarm);
	
	char * charTime = (char*)malloc(13 * sizeof(char));
	sprintf(charTime, "     %02d:%02d     ", newAlarm.HH, newAlarm.MN);
	char tempString[strlen(charTime)];
	
	int i =0;
	for(; i < strlen(charTime);i++)
		tempString[i] = charTime[i];
		
	free(charTime);
	
	return tempString;
}
