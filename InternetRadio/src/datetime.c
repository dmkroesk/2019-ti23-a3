/*
 * datetime.c
 *
 * Created: 19/02/2019 14:54:34
 * Author: Sergen
 */

#define LOG_MODULE  LOG_DATETIME_MODULE

#include <stdio.h>
#include <string.h>
#include <avr/delay.h>

#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>
#include <time.h>

#include "system.h"
#include "portio.h"
#include "log.h"
#include "rtc.h"
#include "x1205.h"
#include "datetime.h"
#include "keyboard.h"
#include "mmc.h"
#include "alarm.h"


void SysMainBeatInterrupt(void *p)
{
	// Scan for valid keys AND check if a MMCard is inserted or removed
	KbScan();
	CardCheckCard();
}

void SysInitIO(void)
{
	/*
	*  Port B:     VS1011, MMC CS/WP, SPI
	*  output:     all, except b3 (SPI Master In)
	*  input:      SPI Master In
	*  pull-up:    none
	*/
	outp(0xF7, DDRB);

	/*
	*  Port C:     Address bus
	*/

	/*
	*  Port D:     LCD_data, Keypad Col 2 & Col 3, SDA & SCL (TWI)
	*  output:     Keyboard colums 2 & 3
	*  input:      LCD_data, SDA, SCL (TWI)
	*  pull-up:    LCD_data, SDA & SCL
	*/
	outp(0x0C, DDRD);
	outp((inp(PORTD) & 0x0C) | 0xF3, PORTD);

	/*
	*  Port E:     CS Flash, VS1011 (DREQ), RTL8019, LCD BL/Enable, IR, USB Rx/Tx
	*  output:     CS Flash, LCD BL/Enable, USB Tx
	*  input:      VS1011 (DREQ), RTL8019, IR
	*  pull-up:    USB Rx
	*/
	outp(0x8E, DDRE);
	outp((inp(PORTE) & 0x8E) | 0x01, PORTE);

	/*
	*  Port F:     Keyboard_Rows, JTAG-connector, LED, LCD RS/RW, MCC-detect
	*  output:     LCD RS/RW, LED
	*  input:      Keyboard_Rows, MCC-detect
	*  pull-up:    Keyboard_Rows, MCC-detect
	*  note:       Key row 0 & 1 are shared with JTAG TCK/TMS. Cannot be used concurrent
	*/
	#ifndef USE_JTAG
	sbi(JTAG_REG, JTD); // disable JTAG interface to be able to use all key-rows
	sbi(JTAG_REG, JTD); // do it 2 times - according to requirements ATMEGA128 datasheet: see page 256

	
	#endif //USE_JTAG

	cbi(OCDR, IDRD);
	cbi(OCDR, IDRD);


	outp(0x0E, DDRF);
	outp((inp(PORTF) & 0x0E) | 0xF1, PORTF);

	/*
	*  Port G:     Keyboard_cols, Bus_control
	*  output:     Keyboard_cols
	*  input:      Bus Control (internal control)
	*  pull-up:    none
	*/
	outp(0x18, DDRG);
}

void SysControlMainBeat(u_char onOff)
{
	int nError = 0;

	if (onOff==ON)
	{
		nError = NutRegisterIrqHandler(&OVERFLOW_SIGNAL, SysMainBeatInterrupt, NULL);
		if (nError == 0)
			init_8_bit_timer();
	}
	else
	{
		// disable overflow interrupt
		disable_8_bit_timer_ovfl_int();
	}
}

void SetNextDay(int DW, int YR, int MO, int DT, int HR, int MIN)
{
	if (MO == 12 && mdays[MO] == DT) // if end of the year
	{
		// To next year
		DW = (DW == 6) ? 0 : DW + 1;
		YR++;
		MO = 1;
		DT = 1;
	}
	else if (MO == 2 && YR % 4 == 0) // if leap year
	{
		if (mdays[MO] + 1 == DT) // if 29th of February
		{
			// To next month
			DW = (DW == 6) ? 0 : DW + 1;
			MO++;
			DT = 1;
		}
		else
		{
			// To next day
			DW = (DW == 6) ? 0 : DW + 1;
			DT++;
		}
	}
	else if (mdays[MO] == DT) // if end of the month
	{
		// To next month
		DW = (DW == 6) ? 0 : DW + 1;
		MO++;
		DT = 1;
	}
	else
	{
		// To next day
		DW = (DW == 6) ? 0 : DW + 1;
		DT++;
	}
	
	tm time;
	X12RtcGetClock(&time);
	
	time.tm_hour = (HR - 24);
	time.tm_min = MIN;
	time.tm_wday = DW;
	time.tm_year = YR;
	time.tm_mon = (MO - 1);
	time.tm_mday = DT;
	
	X12RtcSetClock(&time);
	DetermineNearestAlarm();
}

void SetPreviousDay(int DW, int YR, int MO, int DT, int HR, int MIN)
{
	if (MO == 1 && DT == 1) // if first day of the year
	{
		// To previous year
		DW = (DW == 0) ? 6 : DW - 1;
		YR--;
		MO = 12;
		DT = mdays[MO];
	}
	else if (MO == 3 && YR % 4 == 0) // if leap year
	{
		if (DT == 1) // if 1st of March
		{
			// To previous month
			DW = (DW == 0) ? 6 : DW - 1;
			MO--;
			DT = mdays[MO] + 1;
		}
		else
		{
			// To previous day
			DW = (DW == 0) ? 6 : DW - 1;
			DT--;
		}
	}
	else if (DT == 1) // if first day of the month
	{
		// To previous month
		DW = (DW == 0) ? 6 : DW - 1;
		MO--;
		DT = mdays[MO];
	}
	else
	{
		// To next day
		DW = (DW == 0) ? 6 : DW - 1;
		DT--;
	}
	
	tm time;
	X12RtcGetClock(&time);
	
	time.tm_hour = (HR + 24);
	time.tm_min = MIN;
	time.tm_wday = DW;
	time.tm_year = YR;
	time.tm_mon = MO;
	time.tm_mday = DT;
	
	X12RtcSetClock(&time);
}

void SetX1205Time(int Y2K, int DW, int YR, int MO, int DT, int HR, int MN, int SC)
{
	u_char buffer[10]; // Buffer to assemble data in for sending to x1205 RTC
	buffer[0] = DEC2BCD(SC); // Seconds
	buffer[1] = DEC2BCD(MN); // Minutes
	buffer[2] = DEC2BCD(HR); // Hours
	buffer[3] = DEC2BCD(DT); // Date
	buffer[4] = DEC2BCD(MO); // Month
	buffer[5] = DEC2BCD(YR); // Year (00 - 99)
	buffer[6] = DEC2BCD(DW); // Day of Week (0 - 6)
	buffer[7] = DEC2BCD(Y2K); // Year 2K
	// Send to register 0x30 (RTC SC) and beyond 8 bytes:
	// Seconds, Minutes, Hours, Date, Month, Year, Day of Week, Year 2K
	// See X1205 datasheet Clock/Control Memory Map (page 10)
	x1205WriteNBytes(0x30, buffer, 8);
	NutSleep(100);
}

int GetDOW(void)
{
	struct _tm *currentDateTime = NULL;
	GetDateTime(currentDateTime);
	
	int year = 2000 + currentDateTime->tm_year;
	int month = currentDateTime->tm_mon;
	int day = currentDateTime->tm_mday;
	
	month += 1;
	month = (month + 9) % 12;
	year -= month / 10;
	unsigned long dn = 365*year + year/4 - year/100 + year/400 + (month*306 + 5)/10 + (day - 1);
	
	//Automatically sets Thursday as 0, and Monday as 0 is desired, so +3, %7 ensures this
	int result = ((dn % 7) + 3) % 7;
	
	return result;
}

void GetDateTime(tm * tm)
{
	u_char data[10];
	x1205ReadNByte(0x30, data, 8);
	
	tm->tm_sec = BCD2DEC(data[0] & 0x7F);
	tm->tm_min = BCD2DEC(data[1] & 0x7F);
	tm->tm_hour = BCD2DEC(data[2] & 0x7F);
	tm->tm_mday = BCD2DEC(data[3] & 0x7F);
	tm->tm_mon = BCD2DEC(data[4] & 0x7F) - 1;
	tm->tm_year = BCD2DEC(data[5] & 0x7F);
	tm->tm_wday = BCD2DEC(data[6] & 0x7F);
}

int GetDateTimeSeconds()
{
	u_char data[10];
	x1205ReadNByte(0x30, data, 8);
	
	return BCD2DEC(data[0] & 0x7F);
}

void DateTime_SetTime(int HR, int MN)
{
	tm time;
	X12RtcGetClock(&time);

	time.tm_hour = HR;
	time.tm_min = MN;
	
	X12RtcSetClock(&time);
}

void DateTime_SetDate(int YY, int MM, int DD)
{
	tm time;
	X12RtcGetClock(&time);
	
	time.tm_year = YY;
	time.tm_mon = (MM - 1);
	time.tm_mday = DD;
	
	X12RtcSetClock(&time);
}

char *DateTime_GetDate()
{
	tm * dateTime = NULL;
	GetDateTime(dateTime);
	
	char * date = (char*)malloc(13 * sizeof(char));
	
	sprintf(date, "   20%02d/%02d/%02d", dateTime->tm_year, dateTime->tm_mon+1, dateTime->tm_mday);
	
	return date;
}
