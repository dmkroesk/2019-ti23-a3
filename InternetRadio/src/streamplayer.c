/*
* streamplayer.c
*
* Created: 28/02/2019 08:57:20
* Author: Boudewijn
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include <sys/heap.h>
#include <sys/bankmem.h>
#include <sys/thread.h>
#include <sys/timer.h>

#include "soundcontroller.h"
#include "vs10xx.h"
#include "log.h"
#include "draw.h"
#include "radio.h"
#include "states.h"


int playerState = 0;
//Whether the radio has been kicked
int bufferstate = 0;


THREAD(StreamPlayer, arg)
{
	bufferstate = 0;

	FILE *stream = (FILE *)arg;
	size_t rbytes = 0;
	char *mp3buf;
	int result = NOK;
	int nrBytesRead = 0;
	unsigned char iflag;
	
	// Init MP3 buffer. NutSegBuf is een globale, systeem buffer
	if (0 != NutSegBufInit(8192))
	{
		// Reset global buffer
		iflag = VsPlayerInterrupts(0);
		NutSegBufReset();
		VsPlayerInterrupts(iflag);

		result = OK;
	}

	// Init the Vs1003b hardware
	if (OK == result)
		InitStream();

	for (;;)
	{
		if (playerState == 0)
		{
			NutThreadExit();
			NutThreadDestroy();
		}
		
		// Query number of byte available in MP3 buffer.
		iflag = VsPlayerInterrupts(0);
		mp3buf = NutSegBufWriteRequest(&rbytes);
		VsPlayerInterrupts(iflag);

		// Bij de eerste keer: als player niet draait maak player wakker (kickit)
		if (VS_STATUS_RUNNING != VsGetStatus())
		{
			if (rbytes < 1024)
			{
				bufferstate = 1;
				VsPlayerKick();

				if (GetState() != IMC_STATE_ALARM)
					SetScreenLine2(GetNameMain());
			}
		}

		while (rbytes)
		{
			// Copy rbytes (van 1 byte) van stream naar mp3buf.
			nrBytesRead = fread(mp3buf, 1, rbytes, stream);

			if (nrBytesRead > 0)
			{
				iflag = VsPlayerInterrupts(0);
				mp3buf = NutSegBufWriteCommit(nrBytesRead);
				VsPlayerInterrupts(iflag);
				if (nrBytesRead < rbytes && nrBytesRead < 512)
					NutSleep(250);
			}
			else
				break;
			
			rbytes -= nrBytesRead;

			if (nrBytesRead <= 0)
				break;
		}
	}
	
	SetPlaying(0);

	NutThreadExit();
	NutThreadDestroy();
}

int PlayStream(FILE *stream)
{
	playerState = 1;
	NutThreadCreate("Bg", StreamPlayer, stream, 512);

	return OK;
}

int StopStream()
{
	playerState = 0;
	return VsPlayerStop();
}

int GetBufferState()
{
	return bufferstate;
}
