/*
 * settings.c
 *
 * Created: 21/02/2019 11:10:35
 * Author: Ian
 */

#include "settings.h"
#include "draw.h"
#include "keylogic.h"
#include "storage.h"
#include "states.h"


int settingElement = 0;
int timezoneElement = 0;
char *settingElements[2];
char *timezoneElements[40];


void InitSettings()
{
	InitSettingsElements();
	SetSettingsMenuElement();
}

void InitTimezones()
{
	Flash_TimeSync timeSync;
	ReadTimeSyncFromFlash(&timeSync);
	timezoneElement = timeSync.timezone;
	
	InitTimezoneElements();
	SetTimezoneMenuElement();
}

void SettingsLeft()
{
	settingElement -= 1;
	if(settingElement < 0)
		settingElement = (sizeof(settingElements)/sizeof(char*)) - 1;
	
	SetSettingsMenuElement();
}

void SettingsRight()
{
	settingElement += 1;
	if(settingElement > (sizeof(settingElements)/sizeof(char*)) - 1)
		settingElement = 0;
	
	SetSettingsMenuElement();
}

void TimezonesLeft()
{
	timezoneElement -= 1;
	if(timezoneElement < 0)
		timezoneElement = (sizeof(timezoneElements)/sizeof(char*)) - 1;
	
	SetTimezoneMenuElement();
}

void TimezonesRight()
{
	timezoneElement += 1;
	if(timezoneElement > (sizeof(timezoneElements)/sizeof(char*)) - 1)
		timezoneElement = 0;
	
	SetTimezoneMenuElement();
}

void InitSettingsElements()
{
	settingElements[0] = "<Fabrieksinst.>";
	settingElements[1] = "<  Tijdzone   >";
}

void InitTimezoneElements()
{
	timezoneElements[0] = "<  UTC-12     >";
	timezoneElements[1] = "<  UTC-11     >";
	timezoneElements[2] = "<  UTC-10     >";
	timezoneElements[3] = "<  UTC-9:30   >";
	timezoneElements[4] = "<  UTC-9      >";
	timezoneElements[5] = "<  UTC-8      >";
	timezoneElements[6] = "<  UTC-7      >";
	timezoneElements[7] = "<  UTC-6      >";
	timezoneElements[8] = "<  UTC-5      >";
	timezoneElements[9] = "<  UTC-4      >";
	timezoneElements[10] = "<  UTC-3:30   >";
	timezoneElements[11] = "<  UTC-3      >";
	timezoneElements[12] = "<  UTC-2:30   >";
	timezoneElements[13] = "<  UTC-2      >";
	timezoneElements[14] = "<  UTC-1      >";
	timezoneElements[15] = "<  UTC        >";
	timezoneElements[16] = "<  UTC+1      >";
	timezoneElements[17] = "<  UTC+2      >";
	timezoneElements[18] = "<  UTC+3      >";
	timezoneElements[19] = "<  UTC+3:30   >";
	timezoneElements[20] = "<  UTC+4      >";
	timezoneElements[21] = "<  UTC+4:30   >";
	timezoneElements[22] = "<  UTC+5      >";
	timezoneElements[23] = "<  UTC+5:30   >";
	timezoneElements[24] = "<  UTC+5:45   >";
	timezoneElements[25] = "<  UTC+6      >";
	timezoneElements[26] = "<  UTC+6:30   >";
	timezoneElements[27] = "<  UTC+7      >";
	timezoneElements[28] = "<  UTC+8      >";
	timezoneElements[29] = "<  UTC+8:45   >";
	timezoneElements[30] = "<  UTC+9      >";
	timezoneElements[31] = "<  UTC+9:30   >";
	timezoneElements[32] = "<  UTC+10     >";
	timezoneElements[33] = "<  UTC+10:30  >";
	timezoneElements[34] = "<  UTC+11     >";
	timezoneElements[35] = "<  UTC+12     >";
	timezoneElements[36] = "<  UTC+12:45  >";
	timezoneElements[37] = "<  UTC+13     >";
	timezoneElements[38] = "<  UTC+13:45  >";
	timezoneElements[39] = "<  UTC+14     >";
}

void SetSettingsMenuElement()
{
	SetScreenLine2(settingElements[settingElement]);
}

void SetTimezoneMenuElement()
{
	SetScreenLine2(timezoneElements[timezoneElement]);
}

void SelectSettingsItem()
{
	switch (settingElement)
	{
		case 0:
			ToFactoryReset();
			break;
		case 1:
			ToTimezone();
			break;
	}
}

void SelectTimezoneItem()
{
	Flash_TimeSync timeSync;
	ReadTimeSyncFromFlash(&timeSync);
	
	timeSync.timezone = timezoneElement;
	
	WriteTimeSyncToFlash(timeSync);
	if (GetSyncStatus() == 1)
		SyncTime();
	
	ToSettings();
}

void ToFactoryReset()
{
	SetState(IMC_STATE_MENU_SETTINGS_FACTORY);
	SetScreenLine1(" Fabrieksinst. ");
	SetScreenLine2(" Resetten? [Ok]");
}

void ToTimezone()
{
	SetState(IMC_STATE_MENU_SETTINGS_TIMEZONE);
	SetScreenLine1("   Tijdzone    ");
	InitTimezones();
}

void FactoryReset()
{
	Flash_Alarm alarm = { 00, 00, 0 };
	WriteAlarmToFlash(alarm);
	
	ToSettings();
}
