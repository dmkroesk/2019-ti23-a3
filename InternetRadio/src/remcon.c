/*
* remcon.c
*
* Created: 19/03/2019 11:00:00
* Author: Ricky
*/



#define LOG_MODULE  LOG_REMCON_MODULE

#include <stdlib.h>
#include <fs/typedefs.h>
#include <sys/heap.h>

#include <sys/event.h>
#include <sys/atom.h>
#include <sys/types.h>
#include <dev/irqreg.h>
#include <inttypes.h>

#include "system.h"
#include "portio.h"
#include "remcon.h"
#include "display.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"

const char REMOTE_BUTTON_ONE[]    =	"00011000111001110000100011110111";
const char REMOTE_BUTTON_TWO[]    =	"00011000111001111000100001110111";
const char REMOTE_BUTTON_THREE[]  =	"00011000111001110100100010110111";
const char REMOTE_BUTTON_FOUR[]   = 	"00011000111001111100100000110111";
const char REMOTE_BUTTON_FIVE[]   =	"00011000111001110010100011010111";
const char REMOTE_BUTTON_ALt[]    =	"00011000111001111010100001010111";
const char REMOTE_BUTTON_ESCAPE[] =	"00011000111001111001100001100111";
const char REMOTE_BUTTON_UP[]	  =	"00011000111001110000000011111111";
const char REMOTE_BUTTON_OK[]	  =	"00011000111001111001000001101111";
const char REMOTE_BUTTON_LEFT[]   =	"00011000111001111100000000111111";
const char REMOTE_BUTTON_DOWN[]   =	"00011000111001111000000001111111";
const char REMOTE_BUTTON_RIGHT[]  =	"00011000111001110100000010111111";
const char REMOTE_BUTTON_POWER[]  =	"00011000111001110001000011101111";

/*-------------------------------------------------------------------------*/
/* local variable definitions                                              */
/*-------------------------------------------------------------------------*/
static HANDLE  hRCEvent;

/*-------------------------------------------------------------------------*/
/* local routines (prototyping)                                            */
/*-------------------------------------------------------------------------*/
static void RcInterrupt(int*);
static void RcClearEvent(HANDLE*);

uint16_t test = 0;
int button;
int gettingSignal = 0;
char bits[32];
int bitcounter = 0;
int bitsFull = 0;

/*!
 * \addtogroup RemoteControl
 */

/*@{*/

/*-------------------------------------------------------------------------*/
/*                         start of code                                   */
/*-------------------------------------------------------------------------*/

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief ISR Remote Control Interrupt (ISR called by Nut/OS)
 *
 *  NEC-code consists of 5 parts:
 *
 *  - leader (9 msec high, 4,5 msec low)
 *  - address (8 bits)
 *  - inverted address (8 bits)
 *  - data (8 bits)
 *  - inverted data (8 bits)
 *
 *  The first sequence contains these 5 parts, next
 *  sequences only contain the leader + 1 '0' bit as long
 *  as the user holds down the button
 *  repetition time is 108 msec in that case
 *
 *  Resolution of the 16-bit timer we use here is 4,3 usec
 *
 *  13,5 msecs are 3109 ticks
 *  '0' is 1,25 msecs (260 ticks)
 *  '1' is 2,25 msecs (517 ticks)
 *
 * \param *p not used (might be used to pass parms from the ISR)
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */

static void RcInterrupt(int *p)
{
	// Hier ISR implementeren voor bijv. NEC protocol
	uint16_t timer = TCNT1;
	
	if (test == 0)
	{
		test = timer;
	}
		uint16_t difference = timer - test;
		
		if (gettingSignal == 1)
		{
			if (difference > 400 && difference < 600)
			{
				bits[bitcounter] = '1';
				//printf("%d", 1);
				bitcounter++;
			}
			else if (difference > 200 && difference < 300)
			{
				bits[bitcounter] = '0';
				//printf("%d", 0);
				bitcounter++;
			}
		}
		if (difference > 2900 && difference < 3300 && gettingSignal == 0 )
		{
			gettingSignal = 1;
		}
		
	if (bitcounter >= 32)
	{
		bitsFull = 1;
		bitcounter = 0;
		gettingSignal = 0;
		difference = 0;
		test = 0;
	}
	test = timer;
}

int bitsReady()
{
	return bitsFull;
}
char * getBits()
{
	return bits;
} 

void printRemoteconInfo()
{
	LogMsg_P(LOG_INFO, PSTR("%s"), bits );
}

int GetRemoteButton()
	{
		return button;
	}
void ResetBits()
	{
		bitsFull = 0;
	}
	

void StartRemoteFunction(char *  button)
{	
	//printRemoteconInfo();
	void (*func)();
	
		if (strcmp(button, REMOTE_BUTTON_ONE)==0) 
			func = GetValue(KEY_01);

		else if(strcmp(button, REMOTE_BUTTON_TWO)==0) 
			func = GetValue(KEY_02);
		
		else if(strcmp(button, REMOTE_BUTTON_THREE)==0) 
			func = GetValue(KEY_03);
		
		else if(strcmp(button, REMOTE_BUTTON_FOUR)==0)	
			func = GetValue(KEY_04);
		
		else if(strcmp(button, REMOTE_BUTTON_FIVE)==0) 
			func = GetValue(KEY_05);
		
		else if(strcmp(button, REMOTE_BUTTON_ALt)==0) 
			func = GetValue(KEY_ALT);
		
		else if(strcmp(button, REMOTE_BUTTON_ESCAPE)==0) 
			func = GetValue(KEY_ESC);
		
		else if(strcmp(button, REMOTE_BUTTON_UP)==0) 
			func = GetValue(KEY_UP);

		else if(strcmp(button, REMOTE_BUTTON_OK)==0) 
			func = GetValue(KEY_OK);
		
		else if(strcmp(button, REMOTE_BUTTON_LEFT)==0) 
			func = GetValue(KEY_LEFT);
		
		else if(strcmp(button, REMOTE_BUTTON_DOWN)==0) 
			func = GetValue(KEY_DOWN);
		
		else if(strcmp(button, REMOTE_BUTTON_RIGHT)==0) 
			func = GetValue(KEY_RIGHT);
		
		else if(strcmp(button, REMOTE_BUTTON_POWER)==0) 
			func = GetValue(KEY_POWER);
		else
			func = NULL;
	
	if (func != NULL)
			func();
	
	func = NULL;
}

/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Clear the eventbuffer of this module
 *
 * This routine is called during module initialization.
 *
 * \param *pEvent pointer to the event queue
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
static void RcClearEvent(HANDLE *pEvent)
{
    NutEnterCritical();

    *pEvent = 0;

    NutExitCritical();
}



/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
/*!
 * \brief Initialise the Remote Control module
 *
 * - register the ISR in NutOS
 * - initialise the HW-timer that is used for this module (Timer1)
 * - initialise the external interrupt that inputs the infrared data
 * - flush the remote control buffer
 * - flush the eventqueue for this module
 */
/* 様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様� */
void RcInit()
{
    int nError = 0;

    EICRB &= ~RC_INT_SENS_MASK;    // clear b0, b1 of EICRB

    // Install Remote Control interrupt
    nError = NutRegisterIrqHandler(&sig_INTERRUPT4, RcInterrupt, NULL);
    if (nError == FALSE)
    {
/*
 *  ToDo: control External Interrupt following NutOS calls
#if (NUTOS_VERSION >= 421)
        NutIrqSetMode(&sig_INTERRUPT4, NUT_IRQMODE_FALLINGEDGE);
#else
        EICRB |= RC_INT_FALLING_EDGE;
#endif
        EIMSK |= 1<<IRQ_INT4;         // enable interrupt
 */
        EICRB |= RC_INT_FALLING_EDGE;
        EIMSK |= 1<<IRQ_INT4;         // enable interrupt
    } else {
        LogMsg_P(LOG_ALERT, PSTR("remcon.c 179 Can't register Irq handler"));
    }

    // Initialise 16-bit Timer (Timer1)
    TCCR1B |= (1<<CS11) | (1<<CS10); // clockdivider = 64
    TIFR   |= 1<<ICF1;
    //TIMSK = 1<<TICIE1;

    RcClearEvent(&hRCEvent);
}

/* ---------- end of module ------------------------------------------------ */

/*@}*/
