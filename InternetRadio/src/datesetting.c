/*
 * datesetting.c
 *
 * Created: 21/02/2019 11:43:22
 * Author: Ian
 */ 

#define AMOUNTOFVALUES 6

#include <stdlib.h>
#include <string.h>

#include "datesetting.h"
#include "display.h"
#include "draw.h"
#include "datetime.h"
#include "keylogic.h"
#include "alarm.h"


/*
	Returns whether the current year is a leap year.
	
	@param year - The current year, only last two numbers (20XX).
*/
static int IsLeapYear(int);

/*
	Returns whether the current date is a valid date.
*/
static int IsAllowed(void);


int cursorPos = 0;
int values[AMOUNTOFVALUES];


void InitDateSettings()
{
	int i = 0;
	for(; i < AMOUNTOFVALUES; i++)
		values[i] = 0;
	
	values[3] = 1;
	values[5] = 1;
	
	cursorPos = 0;
}

void DateLeft()
{
	if(cursorPos > 0)
	cursorPos -= 1;
	
	SetCursor();
}

void DateRight()
{	
	if(cursorPos < AMOUNTOFVALUES)
		cursorPos += 1;	
	
	SetCursor();
}

void DateUp()
{
	if(values[cursorPos] < 9)
	{
		values[cursorPos] += 1;
		
		if(IsAllowed() == -1)
			values[cursorPos] -= 1;
	}
	
	SetDateScreen();
}

void DateDown()
{
	if(values[cursorPos] > 0)
	{
		values[cursorPos] -= 1;
		
		if(IsAllowed() == -1)
			values[cursorPos] += 1;
	}	
	
	SetDateScreen();
}

void SetCursor()
{
	if(cursorPos == 0)
		SetScreenLine1("Datum v       ");
	else if (cursorPos == 1)
		SetScreenLine1("Datum  v      ");
	else if (cursorPos == 2)
		SetScreenLine1("Datum    v    ");
	else if (cursorPos == 3)
		SetScreenLine1("Datum     v   ");
	else if (cursorPos == 4)
		SetScreenLine1("Datum       v ");
	else if (cursorPos == 5)
		SetScreenLine1("Datum        v");
}

static int IsLeapYear(int year)
{
	int leapYear = -1;
	
	if((2000 + GetYear()) % 4 == 0) // if divisible by 4
	{
		if((2000 + GetYear()) % 100 > 0) // if not divisible by 100
			leapYear = 0; // is leap year
		else // if divisible by 100
			if((2000 + GetYear()) % 400 == 0) // if divisible by 400
				leapYear = 0;// is leap year
	}
	
	return leapYear;
}

static int IsAllowed()
{
	// Always illegal
	if(GetDay() > 31 || GetDay() < 1 || GetMonth() > 12 || GetMonth() < 1)
		return -1;
		
	// Special cases
	
	// February
	else if(GetMonth() == 2)
	{ 		
		if(IsLeapYear(GetYear()) == 0)
		{
			if(GetDay() > 29)
				return -1;
		}
		else
		{
			if(GetDay() > 28)
				return -1;
		}
	}
	
	//Months with 30 days
	else if(GetMonth() == 4 || GetMonth() == 6 || GetMonth() == 9 || GetMonth() == 11)
		if(GetDay() > 30)
			return -1;
		
	//If it is legal, return 0
	return 0;
}

void SetDateScreen()
{
	char *date = malloc(sizeof(char)*10);
	
	sprintf(date, "    20%d%d/%d%d/%d%d", values[0],values[1],values[2],values[3],values[4],values[5]);
			
	SetScreenLine2(date);
	DrawScreen();
	
	free(date);
}

int GetYear()
{
	int num = 0;
	num += (values[0] * 10);
	num += (values[1] * 1);
	
	return num;
}

int GetMonth()
{
	int num = 0;
	num += (values[2] * 10);
	num += (values[3] * 1);
	
	return num;
}

int GetDay()
{
	int num = 0;
	num += (values[4] * 10);
	num += (values[5] * 1);
	
	return num;
}

void ChangeDate()
{
	DateTime_SetDate(GetYear(), GetMonth(), GetDay());
	char *temp = DateTime_GetDate();
	SetScreenLine2(temp);
	free(temp);
	
	DetermineNearestAlarm();
	
	ToMenu();
}
