/*
 * radio.c
 *
 * Created: 15/03/2019 10:02:18
 * Author: Boudewijn
 */

#define LOG_MODULE LOG_MAIN_MODULE

#include "log.h"
#include "radio.h"
#include "soundcontroller.h"


RadioChannel radioList[3];
int mainRadioElement;
int alarmRadioElement;
int playing;


void InitRadioList()
{
    RadioChannel channel0 = {"     Jazz      ", "50.7.98.106", 8465};
    radioList[0] = channel0; // Jazz
    RadioChannel channel1 = {"     C-Rock    ", "198.58.98.83", 8258};
    radioList[1] = channel1; // C-Rock
    RadioChannel channel2 = {"   Aardschok   ", "178.18.137.248", 80};
    radioList[2] = channel2; // Aardschok

    mainRadioElement = 0;
    alarmRadioElement = 0;
    playing = 0;
}

void StartMainRadio()
{
    int success = OpenStream(radioList[mainRadioElement], "/");
    if (success == 0)
        PlayStream(GetStream());
}

void StartAlarmRadio()
{
    int success = OpenStream(radioList[alarmRadioElement], "/");
    if (success == 0)
        PlayStream(GetStream());
}

void SetRadioAlarm(int channel)
{
    alarmRadioElement = channel;
}

void SetMainRadio(int channel)
{
    mainRadioElement = channel;
}

int GetMainChannel()
{
    return mainRadioElement;
}

int GetPlaying()
{
    return playing;
}

char *GetNameMain()
{
    return radioList[mainRadioElement].name;
}

void SetPlaying(int status)
{
    playing = status;
}

void AddRadioChannel(RadioChannel *channel)
{
	// TODO: Increase list size and add channel.

}
