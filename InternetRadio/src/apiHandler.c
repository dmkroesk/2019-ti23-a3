/*
* apiHandler.c
*
* Created: 3/20/2019 7:10:46 PM
* Author: Ruben
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include "json_parser.h"
#include "apiHandler.h"
#include "httpRequest.h"
#include "datetime.h"
#include "radio.h"
#include "alarm.h"
#include "storage.h"


int RequestConfig()
{
	char *response;
	ApiConfig *config;

	response = RequestGet("178.62.201.195", 80, "/config/123", "");
	LogMsg_P(LOG_INFO, PSTR("Response: %s"), response);
	LogMsg_P(LOG_INFO, PSTR("Attempting to unmarshall response: \n%s"), response);

	config = CreateConfigFromString(response, strlen(response));
	free(response); // Free response after creating tokens from the string.
	PrintConfig(config);
	return ApplyConfig(config);
}

// NOTE: return ApiConfig pointer on
ApiConfig *CreateConfigFromString(char *json_content, int json_length)
{
	ApiConfig *config = (ApiConfig *)malloc(sizeof(ApiConfig));

	// Parse ID 
	int res_code = ParseJSON(json_content, "{\"id\":\"", "\",\"datetime_settings\":{\"", &config->id);

	// Parse DateTime Hour
	char *buff;
	res_code = ParseJSON(json_content, ":{\"datetime_hour\":", ",\"datetime_minute\"", &buff);
	config->dt_settings.time_hour = atoi(buff);
	buff = "";
	// Parse DateTime Minute
	res_code = ParseJSON(json_content, "datetime_minute\":", ",\"date_year\"", &buff);
	config->dt_settings.time_minute = atoi(buff);
	buff = "";
	// Parse DateTime Year
	res_code = ParseJSON(json_content, "\"date_year\":", ",\"date_month\"", &buff);
	config->dt_settings.date_year = atoi(buff);
	buff = "";
	// Parse DateTime Month
	res_code = ParseJSON(json_content, "\"date_month\":", ",\"date_day\"", &buff);
	config->dt_settings.date_month =atoi(buff);
	buff = "";

	// Parse DateTime Day
	res_code = ParseJSON(json_content, "\"date_day\":", ",\"timezone\"", &buff);
	config->dt_settings.date_day = atoi(buff);
	buff = "";
	// Parse DateTime timezone
	res_code = ParseJSON(json_content, "\"timezone\":", ",\"timesync\"", &buff);
	config->dt_settings.timezone = atoi(buff);
	buff = "";
	// Parse DateTime timesync
	res_code = ParseJSON(json_content, "\"timesync\":", "},\"audio_settings\":{", &buff);
	config->dt_settings.timesync = atoi(buff);
	buff = "";
	// Parse Audio Volume
	res_code = ParseJSON(json_content, "\"volume\":", ",\"treble\":", &buff);
	config->audio_settings.volume = atoi(buff);
	buff = "";

	// Parse Audio treble
	res_code = ParseJSON(json_content, "\"treble\":", ",\"bass\"", &buff);
	config->audio_settings.treble = atoi(buff);
	buff = "";

	// Parse Audio bass
	res_code = ParseJSON(json_content,  "\"bass\":", " },\"radio_settings\"", &buff);
	config->audio_settings.bass = atoi(buff);
	buff = "";
	// Parse Radio IP address
	res_code = ParseJSON(json_content, "radio_settings\":{\"ip_address\":\"", "\",\"port\"", &config->radio_settings.ip_address);
	config->radio_settings.channel_name = "Jazz";

	// Parse Radio Port
	res_code = ParseJSON(json_content, "\"port\":\"", "\",\"route\"", &buff);
	config->radio_settings.port = atoi(buff);
	buff = "";

	// Parse Radio Route
	res_code = ParseJSON(json_content, ",\"route\"", "},\"alarm_settings\":", &config->radio_settings.route);

	// Parse Alarm Hour
	res_code = ParseJSON(json_content, "\"alarm_time_hour\":", ",\"alarm_time_minute\"", &buff);
	config->alarm_settings.time_hour =atoi(buff);
	buff = "";

	// Parse Alarm Minute
	res_code = ParseJSON(json_content, "\"alarm_time_minute\":", ",\"re_arm\":", &buff);
	config->alarm_settings.time_minute = atoi(buff);
	buff = "";
	// Parse Alarm re_arm
	res_code = ParseJSON(json_content, "\"re_arm\":", ",\"audio_type\"", &buff);
	config->alarm_settings.re_arm = atoi(buff);
	buff = "";
	// Parse Alarm audio_type
	res_code = ParseJSON(json_content, "\"audio_type\":", ",\"radio_channel\"", &buff);
	config->alarm_settings.audio_type = atoi(buff);
	buff = "";
	// Parse alarm radio_channel
	res_code = ParseJSON(json_content, "\"radio_channel\":", "},\"general_settings\":", &buff);
	config->alarm_settings.radio_channel = atoi(buff);
	buff = "";
	// Parse factory settings
	res_code = ParseJSON(json_content, "\"factory_settings\"", "}}", &buff);
	config->general_settings.factory_settings = atoi(buff);
	buff = "";

	/*
	// Hardcoded settings for testing
	config->id = "123";
	config->dt_settings.time_hour = 12;
	config->dt_settings.time_minute = 58;
	config->dt_settings.date_year = 19;
	config->dt_settings.date_month = 3;
	config->dt_settings.date_day = 19;
	config->dt_settings.timezone = 1;
	config->dt_settings.timesync = 0;
	config->audio_settings.volume = 10;
	config->audio_settings.treble = 5;
	config->audio_settings.bass = 15;
	config->radio_settings.ip_address = "123.123.123.123";
	config->radio_settings.channel_name = "Jazz";
	config->radio_settings.port = 80;
	config->radio_settings.route = "/";
	config->alarm_settings.time_hour = 13;
	config->alarm_settings.time_minute = 1;
	config->alarm_settings.re_arm = 1;
	config->alarm_settings.audio_type = 0;
	config->alarm_settings.radio_channel = 1;
	config->general_settings.factory_settings = 0;
*/
	return config;
}

// VerifyId verifies the given ID with the device. return 0 on success or -1 on failure.
static int VerifyId(int id)
{
	return 0; // TODO: verify ID
}

// Applies date & time settings of the device. return 0 on success or -1 on failure.
static int ApplyDateTimeSettings(DateTime_Settings settings)
{
	// TODO: With more time add check if it was actually applied correctly.
	DateTime_SetTime(settings.time_hour, settings.time_minute);
	DateTime_SetDate(settings.date_year, settings.date_month, settings.date_day);
	// TODO: Set Timezone after merge with Sergen.
	// TODO: Set Timesync after merge with Sergen.
	return 0;
}
// Applies audio settings of the device. return 0 on success or -1 on failure.
static int ApplyAudioSettings(Audio_Settings settings)
{
	// TODO: With more time add check if it was actually applied correctly.
	// TODO: Apply after merge with Bou.
	return 0;
}

// Applies radio settings of the device. return 0 on success or -1 on failure.
static int ApplyRadioSettings(Radio_Settings settings)
{
	RadioChannel *channel = (RadioChannel *)malloc(sizeof(RadioChannel));
	if (channel == NULL)
	{
		return -1;
	}
	channel->name = settings.channel_name;
	channel->url = settings.ip_address;
	channel->port = settings.port;

	// TODO: With more time add check if it was actually applied correctly.
	AddRadioChannel(channel);
	free(channel);
	return 0;
}

// Applies alarm settings of the device. return 0 on success or -1 on failure.
static int ApplyAlarmSettings(Alarm_Settings settings)
{
	// TODO: With more time add check if it was actually applied correctly.
	SetAlarmTime(settings.time_hour, settings.time_minute);
	if (settings.re_arm == 0)
	{
		SetReArmOff();
	}
	else
	{
		SetReArmOn();
	}

	if (settings.audio_type == 0)
	{
		SetRadioOff();
	}
	else
	{
		SetRadioOn();
	}
	SetRadioAlarm(settings.radio_channel);

	return 0;
}

// Applies general settings of the device. return 0 on success or -1 on failure.
static int ApplyGeneralSettings(General_Settings settings)
{
	// TODO: With more time add check if it was actually applied correctly.
	if (settings.factory_settings == 1)
	{
		ResetFlashStorage();
	}
	return 0;
}

// NOTE: return 0 on success or negative number indicating an error.
int ApplyConfig(ApiConfig *cfg)
{
	if (VerifyId(cfg->id) != 0)
	{
		return -6;
	}
	if (ApplyDateTimeSettings(cfg->dt_settings) != 0)
	{
		return -7;
	}
	if (ApplyAudioSettings(cfg->audio_settings) != 0)
	{
		return -8;
	}
	if (ApplyRadioSettings(cfg->radio_settings) != 0)
	{
		return -9;
	}
	if (ApplyAlarmSettings(cfg->alarm_settings) != 0)
	{
		return -10;
	}
	if (ApplyGeneralSettings(cfg->general_settings) != 0)
	{
		return -11;
	}
	FreeApiConfig(cfg); // Free the ApiConfig after applying the configuration to the system.
	return 0;
}

int FreeApiConfig(ApiConfig *cfg)
{
	free(cfg->id);
	free(cfg->radio_settings.ip_address);
	free(cfg->radio_settings.channel_name);
	free(cfg->radio_settings.route);
	free(cfg);
}

char *GetApiError(int error_code)
{
	char *err;
	switch (error_code)
	{
	case -1:
		err = "error processing HTTP request";
		return err;
	case -2:
		err = "error parsing data; bad token, JSON string is corrupted";
		return err;
	case -3:
		err = "error parsing data; not enough tokens, JSON string is too large";
		return err;
	case -4:
		err = "error parsing data; JSON string is too short, expecting more JSON data";
		return err;
	case -5:
		err = "error creating config from tokens";
		return err;
	case -6:
		err = "error verifying the ID";
		return err;
	case -7:
		err = "error applying date time settings to IMC device";
		return err;
	case -8:
		err = "error applying audio settings to IMC device";
		return err;
	case -9:
		err = "error applying radio settings to IMC device";
		return err;
	case -10:
		err = "error applying alarm settings to IMC device";
		return err;
	case -11:
		err = "error applying general settings to IMC device";
		return err;
	default:
		return "error not defined";
	}
}

void PrintConfig(ApiConfig *config)
{
	LogMsg_P(LOG_INFO, PSTR("%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%s,%d,%s,%d,%d,%d,%d,%d,%d,"), config->id,
			 config->dt_settings.time_hour,
			 config->dt_settings.time_minute,
			 config->dt_settings.date_year,
			 config->dt_settings.date_month,
			 config->dt_settings.date_day,
			 config->dt_settings.timezone,
			 config->dt_settings.timesync,
			 config->audio_settings.volume,
			 config->audio_settings.treble,
			 config->audio_settings.bass,
			 config->radio_settings.ip_address,
			 config->radio_settings.channel_name,
			 config->radio_settings.port,
			 config->radio_settings.route,
			 config->alarm_settings.time_hour,
			 config->alarm_settings.time_minute,
			 config->alarm_settings.re_arm,
			 config->alarm_settings.audio_type,
			 config->alarm_settings.radio_channel,
			 config->general_settings.factory_settings);
}