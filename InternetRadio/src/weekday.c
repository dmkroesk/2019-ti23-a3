/* 
 * weekday.c
 * 
 * Created: 19-3-2019 17:00:57
 * Author: Ian
 */

#include "weekday.h"
#include "draw.h"
#include "storage.h"
#include "alarm.h"


// Value of what the cursor is pointing is at
int currentValue;

// The values in the menu that can be changed
int values[5];

// The string of the day
char *dayString;

// Index of the current weekday
int weekDayIndex;


void InitWeekDay(int day)
{
	weekDayIndex = day;
	currentValue = 0;
	
	Flash_WeekAlarm fWeekAlarm;
	ReadWeekAlarmFromFlash(&fWeekAlarm);
	
	Flash_Alarm selectedAlarm = fWeekAlarm.alarms[weekDayIndex];
	
	values[0] = selectedAlarm.HH / 10;
	values[1] = selectedAlarm.HH % 10;
	values[2] = selectedAlarm.MN / 10;
	values[3] = selectedAlarm.MN % 10;
	values[4] = selectedAlarm.alarmStatus;
	
	switch(day)
	{
		case 0:
			dayString = "Ma";
			break;
		case 1: 
			dayString = "Di";
			break;
		case 2: 
			dayString = "Wo";
			break;
		case 3: 
			dayString = "Do";
			break;
		case 4: 
			dayString = "Vr";
			break;
		case 5: 
			dayString = "Za";
			break;
		case 6: 
			dayString = "Zo";
			break;
	}
	
	SetWeekDayCursor();
	SetWeekDayScreen();
}

void Weekday_Left(void)
{
	currentValue --;
	if(currentValue < 0)
		currentValue = 0;
	
	SetWeekDayCursor();
}

void Weekday_Right(void)
{
	currentValue ++;
	if(currentValue > (sizeof(values)/sizeof(int) - 1))
		currentValue = (sizeof(values)/sizeof(int) - 1);
	
	SetWeekDayCursor();
}

void Weekday_Up()
{
	if(values[currentValue] < 9)
	{
		values[currentValue] += 1;
		
		if(Weekday_IsAllowed() == -1)
			values[currentValue] -= 1;
	}
	
	SetWeekDayScreen();
}

void Weekday_Down()
{
	if(values[currentValue] > 0)
	{
		values[currentValue] -= 1;
		
		if(Weekday_IsAllowed() == -1)
			values[currentValue] += 1;
	}
	
	SetWeekDayScreen();
}

/*
	Checks if the current value is possible for the  value is possible.
*/
int Weekday_IsAllowed()
{
	// Illegal values
	if(GetHours() > 23 || GetMinutes() > 59 || values[4] > 1)
		return -1;
	
	return 0;
}

void SetWeekDayCursor()
{
	char *line1 = malloc(sizeof(char)*15);
	
	if(currentValue == 0)
		sprintf(line1, "%s v     Arm   ", dayString);
	else if (currentValue == 1)
		sprintf(line1, "%s  v    Arm   ", dayString);
	else if (currentValue == 2)
		sprintf(line1, "%s    v  Arm   ", dayString);
	else if (currentValue == 3)
		sprintf(line1, "%s     v Arm   ", dayString);
	else if (currentValue == 4)
		sprintf(line1, "%s       Arm v ", dayString);
	
	SetScreenLine1(line1);
	
	free(line1);
}

void SetWeekDayScreen()
{
	char *weekalarm = malloc(sizeof(char)*15);
	
	sprintf(weekalarm, "   %d%d:%d%d     %d ", values[0],values[1],values[2],values[3],values[4]);
	
	SetScreenLine2(weekalarm);
	DrawScreen();
	
	free(weekalarm);
}

int GetHours(void)
{
	int sum = 0;
	sum += values[0] * 10;
	sum += values[1];
	
	return sum;
}

int GetMinutes(void)
{
	int sum = 0;
	sum += values[2] * 10;
	sum += values[3];
	
	return sum;
}

void ConfirmWeekDaySettings(void)
{
	Flash_WeekAlarm fWeekAlarm;
	ReadWeekAlarmFromFlash(&fWeekAlarm);
	
	Flash_Alarm selectedAlarm = { GetHours(), GetMinutes(), values[4] };
	
	fWeekAlarm.alarms[weekDayIndex] = selectedAlarm;
	
	WriteWeekAlarmToFlash(fWeekAlarm);
	UpdateWeekAlarm(fWeekAlarm);
	DetermineNearestAlarm();
	
	ToWeekAlarm();
}
