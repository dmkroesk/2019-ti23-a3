/*
* keylogic.c
*
* Created: 18/02/2019 20:14:52
* Author: Ian
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "keylogic.h"
#include "states.h"
#include "alarm.h"
#include "draw.h"
#include "watchdog.h"
#include "settings.h"
#include "display.h"
#include "alarmsetting.h"
#include "streamplayer.h"
#include "radio.h"
#include "netcomponent.h"
#include "networkmenu.h"
#include "alarmmenu.h"
#include "datesetting.h"
#include "weekalarms.h"
#include "soundcontroller.h"


int alarmState = 0;
int menuElement = 0;
char *menuElements[6];


void TurnOff()
{
	//SetState(IMC_STATE_OFF);
	//SetScreenLine1("----------------");
	//SetScreenLine2("----------------");
	WatchDogEnable();
	WatchDogStart(30);
	SetAlarmMenuElement();
	{};
}

void ToMain()
{
	SetState(IMC_STATE_MAIN);

	if (GetPlaying() == 1)
	{
		if (GetBufferState() == 1)
			SetScreenLine2(GetNameMain());
		else
			SetScreenLine2("   Loading...  ");
	}

	UpdateDateTime();
	DrawScreen();
}

void ToMenu()
{
	SetState(IMC_STATE_MENU);
	InitMenuElements();
	SetScreenLine1("    Menu       ");
	SetMenuElement();
}

void ToAlarmMenu()
{
	SetState(IMC_STATE_MENU_ALARM);
	InitAlarmMenuElements();
	SetScreenLine1("   Alarm menu  ");
	SetAlarmMenuElement();
}

void ToAlarmTime()
{
	SetState(IMC_STATE_MENU_ALARM_TIME);
	SetScreenLine1("Alarm v         ");
	SetScreenLine2("      00:00     ");
	InitAlarmSetting();
}

void ToAlarmReArm()
{
	SetState(IMC_STATE_MENU_ALARM_REARM);
	SetScreenLine1("      Re-arm    ");
	SetReArmScreen();
}

void ToAlarmSound()
{
	SetState(IMC_STATE_MENU_ALARM_SOUND);
	SetScreenLine1("    Geluid     ");
	SetSoundScreen();
}

void ToTime()
{
	SetState(IMC_STATE_MENU_TIME);
	SetScreenLine1("Tijd v         ");
	SetScreenLine2("     00:00     ");
}

void ToDate()
{
	SetState(IMC_STATE_MENU_DATE);
	SetScreenLine1("Datum v        ");
	SetScreenLine2("    2000/01/01 ");
	InitDateSettings();
}

void ToSettings()
{
	SetState(IMC_STATE_MENU_SETTINGS);
	SetScreenLine1("  Instellingen ");
	InitSettings();
}

void ToNetwork()
{
	SetState(IMC_STATE_MENU_NETWORK);
	InitNetworkMenuElements();
	SetScreenLine1("    Netwerk    ");
	SetNetworkMenuElement();
}

void ToMAC()
{
	SetState(IMC_STATE_MENU_NETWORK_MAC);
	SetScreenLine1("MAC v       1-3");
	SetScreenLine2("<   00:00:00  >");
	InitMacSettings();
}

void ToTimeSync()
{
	SetState(IMC_STATE_MENU_NETWORK_TIMESYNC);
	InitTimeSync();
	SetScreenLine1("Tijd synchroni.");
	SetTimeSyncMenuElement();
}

void ToAlarmRadio()
{
	SetState(IMC_STATE_MENU_ALARM_RADIO);
	InitAlarmRadioMenuElements();
	SetScreenLine1("Radio channel  ");
	SetAlarmRadioMenuElement();
}

void ToWeekAlarm(void)
{
	SetState(IMC_STATE_MENU_ALARM_WEEK);
	InitWeekAlarmElements();
	SetScreenLine1("Week alarmen  ");
	InitWeekAlarmMenu();
}

void ToVolume()
{
	SetState(IMC_STATE_MENU_AUDIO_VOLUME);
	DrawVolume(GetVolume());
}

void ToAudioMenu()
{
	SetState(IMC_STATE_MENU_AUDIO);
	InitAudioMenuElements();
	SetScreenLine1("  Audio        ");
	SetAudioMenuElement();
}

void ToAudioBassMenu()
{
	SetState(IMC_STATE_MENU_AUDIO_BASS);
	DrawBass(GetBass());
	SetScreenLine1("   Bass        ");
}


void ToAudioTrebleMenu()
{
	SetState(IMC_STATE_MENU_AUDIO_TREBLE);
	DrawTreble(GetTreble());
	SetScreenLine1("   Treble      ");
}

void MenuLeft()
{
	menuElement--;
	if (menuElement < 0)
		menuElement = 5;

	SetMenuElement();
}

void MenuRight()
{
	menuElement++;
	if (menuElement > 5)
		menuElement = 0;

	SetMenuElement();
}

void SetMenuElement()
{
	SetScreenLine2(menuElements[menuElement]);
}

void SelectItem()
{
	switch (menuElement)
	{
		case 0:
			ToAlarmMenu();
			break;
				
		case 1:
			ToAudioMenu();
			break;

		case 2:
			ToTime();
			break;

		case 3:
			ToDate();
			break;

		case 4:
			ToSettings();
			break;

		case 5:
			ToNetwork();
			break;
	}
}

void Awake()
{
	SetState(IMC_STATE_MAIN);
}

void InitMenuElements()
{
	menuElements[0] = "<   Alarm     >";
	menuElements[1] = "<   Audio     >";
	menuElements[2] = "<   Tijd      >";
	menuElements[3] = "<   Datum     >";
	menuElements[4] = "<   Instell.  >";
	menuElements[5] = "<   Netwerk   >";
}

void SwitchAlarm()
{
	OnOffSwitch();
}

void SwitchRadio()
{
	if (GetInternetStatus() == 1)
	{
		if (GetPlaying() == 1)
			RadioOff();
		else
			RadioOn();
	}
}

void RadioOn()
{
	SetPlaying(1);
	NutSleep(50);
	SetScreenLine2("   Loading...  ");
	UpdateDateTime();
	DrawScreen();
	StartMainRadio();
}

void RadioOff()
{
	SetPlaying(0);
	StopStream();
}

void SwitchChannelLeft()
{
	if (GetInternetStatus() == 1)
	{
		SetScreenLine2("   Loading...  ");
		UpdateDateTime();
		DrawScreen();
		
		if (GetPlaying() == 1)
			StopStream();
		
		SetMainRadio(GetMainChannel() - 1);
		
		if (GetMainChannel() < 0)
			SetMainRadio(2);
		
		SetPlaying(1);
		StartMainRadio();
	}
}

void SwitchChannelRight()
{
	if (GetInternetStatus() == 1)
	{
		SetScreenLine2("   Loading...  ");
		UpdateDateTime();
		DrawScreen();
		
		if (GetPlaying() == 1)
			StopStream();
		
		SetMainRadio(GetMainChannel() + 1);
		
		if (GetMainChannel() > 2)
			SetMainRadio(0);
		
		SetPlaying(1);
		StartMainRadio();
	}
}

void InternetOn()
{
	char *templine = GetScreenLine1();
	templine[15] = 'T';

	SetScreenLine2(templine);
}

void InternetOff()
{
	char *templine = GetScreenLine1();
	templine[15] = ' ';

	SetScreenLine2(templine);
}

void AlarmSnooze()
{
	AlarmDone();
	Snooze();
}

void AlarmDone()
{
	ToMain();
	RadioOff();
	AlarmOff();
}

int GetAlarmState()
{
	return alarmState;
}

void VolumeUp()
{
	UpdateVolume(1);
	DrawVolume(GetVolume());
}

void VolumeDown()
{
	UpdateVolume(-1);
	DrawVolume(GetVolume());
}

void BassUp()
{
	UpdateBass(1);
	DrawBass(GetBass());
}

void BassDown()
{
	UpdateBass(-1);
	DrawBass(GetBass());

}

void TrebleUp()
{
	UpdateTreble(1);
	DrawTreble(GetTreble());
}

void TrebleDown()
{
	UpdateTreble(-1);
	DrawTreble(GetTreble());
}
