/*
 * timesetting.c
 *
 * Created: 02/21/2019 11:47:06 AM
 * Author: Ruben
 */

#define LOG_MODULE LOG_MAIN_MODULE

#include <stdio.h>
#include <string.h>
#include <avr/delay.h>
#include <sys/thread.h>
#include <sys/timer.h>
#include <sys/version.h>
#include <dev/irqreg.h>

#include "system.h"
#include "display.h"
#include "keyboard.h"
#include "led.h"
#include "log.h"
#include "flash.h"
#include "rtc.h"
#include "datetime.h"
#include "watchdog.h"
#include "x1205.h"
#include "remcon.h"
#include "uart0driver.h"
#include "mmc.h"
#include "spidrv.h"
#include "draw.h"
#include "timesetting.h"
#include "alarm.h"


static void Redraw(void);
static void Check(void);


int position = 0;
int timeSetter[] = { 0, 0, 0, 0 };


unsigned Concatenate(unsigned x, unsigned y)
{
	unsigned pow = 10;
	while(y >= pow)
		pow *= 10;
	
	return x * pow + y;
}

static void Check()
{
	int i = 0;
	for (; i < 4; i++)
		if (timeSetter[i] > 9)
			timeSetter[i] = 0;
	
	if (Concatenate(timeSetter[0], timeSetter[1]) > 23)
	{
		timeSetter[0] = 0;
		timeSetter[1] = 0;
	}
	
	if (Concatenate(timeSetter[2], timeSetter[3]) > 59)
	{
		timeSetter[2] = 0;
		timeSetter[3] = 0;
	}
}

static void Redraw()
{
	Check();
	char *tt = malloc(sizeof(char) * 16);

	sprintf(tt, "     %d%d:%d%d     ", timeSetter[0], timeSetter[1], timeSetter[2], timeSetter[3]);
	
	switch(position)
	{
		case 0:
			SetScreenLine1("Tijd v         ");
			break;
			
		case 1:
			SetScreenLine1("Tijd  v        ");
			break;
			
		case 2:
			SetScreenLine1("Tijd    v      ");
			break;
			
		case 3:
			SetScreenLine1("Tijd     v       ");
			break;
			
		default:
			break;
	}
	
	SetScreenLine2(tt);
	free(tt);
}

void TimeSettingLeft()
{
	if (position < 0)
		position = 0;
	else
		position--;
	
	Redraw();
}

void TimeSettingRight()
{
	if (position > 3)
		position = 3;
	else
		position++;
	
	Redraw();
}

void TimeSettingUp()
{
	timeSetter[position]++;
	Redraw();
}

void TimeSettingDown()
{
	if (timeSetter[position] > 0)
		timeSetter[position]--;
	
	Redraw();
}

void TimeSettingConfirm()
{
	int hh = Concatenate(timeSetter[0], timeSetter[1]);
	int mm = Concatenate(timeSetter[2], timeSetter[3]);
	DateTime_SetTime(hh, mm);
	
	position = 0;
	int i = 0;
	for(; i < 4; i++)
		timeSetter[i] = 0;
	
	DetermineNearestAlarm();
	ToMenu();
}
