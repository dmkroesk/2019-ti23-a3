/*
* soundcontroller.c
*
* Created: 28/02/2019 11:18:43
*  Author: Bou's Laptop
*/
#define LOG_MODULE LOG_MAIN_MODULE
#include <sys/thread.h>
#include <sys/types.h>
#include <sys/timer.h>
#include <string.h>
#include <sys/heap.h>
#include "vs10xx.h"
#include "log.h"
#include "soundcontroller.h"
#include "alarm.h"
#include "keylogic.h"
#include "states.h"
#include "streamplayer.h"
#include "storage.h"

int volume;
int audioElement = 0;
char *audioElements[1];
int treble;
int bass;

THREAD(playtoneThread, args)
{

	int i = 0;
	int ii = 1;
	
	for (;;)
	{
		VsPlayerInit();
		VsBeep(((ii * 10) * (i + 1)), 1000);
		VsPlayerStop();

		ii++;
		if (ii == 7)
		ii = 1;

		if (GetState() == IMC_STATE_MAIN)
		break;

		NutSleep(10);
	}
	LogMsg_P(LOG_INFO, PSTR("Exiting Thread!!!!!!\n"));
	NutSleep(500);
	NutThreadExit();
	NutThreadDestroy();
}

void PlayTone()
{
	LogMsg_P(LOG_INFO, PSTR("Starting playTone thread!!!!!!\n"));

	NutThreadCreate("playtoneThread", playtoneThread, NULL, 512);
}

void InitAudioMenuElements()
{
	audioElements[0] = ("<    Volume   >");
	audioElements[1] = ("<    Bass     >");
	audioElements[2] = ("<    Treble   >");
}

void SetAudioMenuElement(){
	SetScreenLine2(audioElements[audioElement]);
}

void SelectAudioItem()
{
	switch (audioElement)
	{
		case 0:
		ToVolume();
		break;

		case 1:
		ToAudioBassMenu();
		break;

		case 2:
		ToAudioTrebleMenu();
		break;
	}
}


void AudioLeft()
{
	audioElement--;
	if(audioElement < 0)
	audioElement = 2;
	SetAudioMenuElement();
}

void AudioRight()
{
	audioElement++;
	if(audioElement > 2)
	audioElement = 0;
	SetAudioMenuElement();
}

void SilentRadio()
{
	VsSetVolume(254, 254);
}

int CalculateVolume()
{
	int newVolume = 254;
	if(volume == 20)
	{
		newVolume = 0;
	} else
	{
		newVolume = newVolume - (volume * 6 + 120);
	}
	return newVolume;
}

int CalculateTreble()
{
	switch(treble)
	{
		case -1:
		return 15;
		break;
		case -2:
		return 14;
		break;
		case -3:
		return 13;
		break;
		case -4:
		return 12;
		break;
		case -5:
		return 11;
		break;
		case -6:
		return 10;
		break;
	}
	return treble;
}

u_char CalculateBassTreble()
{
	u_char new = (CalculateTreble() << 12) +  0 + (bass << 12) + 0;
	return new;
}

int UpdateBassRegister()
{
	u_char ief;
	ief = VsPlayerInterrupts(0);
	VsRegWrite(VS_BASS_REG, CalculateBassTreble());
	VsPlayerInterrupts(ief);
	return(0);
}

void UpdateVolume(int update){
	Flash_Volume volumenew;
	int newVolume = 0;
	if(update < 0 && volume > 0)
	{
		volume--;
	} else if(update > 0 && volume < 20)
	{
		volume++;
	}else
	{
		return;
	}
	newVolume = CalculateVolume();
	volumenew.volume = volume;
	VsSetVolume(newVolume, newVolume);
	WriteVolumeToFlash(volumenew);
}

void UpdateTreble(int update)
{
	Flash_Treble trebleFlash;
	if(treble < 6 && update > 0){
		treble++;
		//VsSetBassReg(newtreble);
	} else if (treble > -6 && update < 0)
	{
		treble--;
	}
	
	trebleFlash.treble = treble;
	WriteTrebleToFlash(trebleFlash);
}

void UpdateBass(int update)
{
	Flash_Bass bassFlash;
	if(bass < 14 && update > 0){
		bass++;
		//VsSetBassReg(newtreble);
	} else if (bass > 0 && update < 0)
	{
		bass--;
	}
	
	
	bassFlash.bass = bass;
	WriteBassToFlash(bassFlash);
}

int GetVolume()
{
	return volume;
}

int GetBass()
{
	return bass;
}

int GetTreble()
{
	return treble;
}

int InitStream()
{
	Flash_Volume volumenew;
	Flash_Bass bassnew;
	Flash_Treble treblenew;
	
	ReadVolumeFromFlash(&volumenew);
	ReadBassFromFlash(&bassnew);
	ReadTrebleFromFlash(&treblenew);
	
	//VsSetBassReg(bassnew.bass);
	UpdateBassRegister();
	
	volume = volumenew.volume;
	treble = treblenew.treble;
	bass = bassnew.bass;
	
	//VsSetBassReg(0x4040);
	int result = OK;
	if (-1 == VsPlayerInit())
	{
		if (-1 == VsPlayerReset(0))
		{
			result = NOK;
		}
	}
	VsSetVolume(134, 134);
	
	return result;
}