/*
* httpRequest.c
*
* Created: 3/19/2019 1:35:01 PM
* Author: Ruben
*/

#define LOG_MODULE LOG_MAIN_MODULE

#include <sys/thread.h>

#include "httpRequest.h"
#include "netcomponent.h"
#include "log.h"

static char *ConvertStreamToString(FILE *stream)
{
	char *buffer = 0;
	long length;

	if (stream)
	{
		fseek(stream, 0, SEEK_END);
		length = ftell(stream);
		fseek(stream, 0, SEEK_SET);
		buffer = malloc(length);
		if (buffer)
		{
			fread(buffer, 1, length, stream);
		}
	}
	return buffer;

	/*if (buffer)
	{
	// start to process your data / extract strings here...
	}*/
}

THREAD(request_thread, args)
{
}

int RequestSendAsync(Methods method, char *addr, int port, char *route, char *body, void (*callback)(int))
{
	NutThreadCreate("request_thread", request_thread, NULL, 512);
}

int RequestGetAsync(char *addr, int port, char *route, char *body, void (*callback)(int))
{
	RequestSendAsync(GET, addr, port, route, body, callback);
}

int RequestPostAsync(char *addr, int port, char *route, char *body, void (*callback)(int))
{
	RequestSendAsync(POST, addr, port, route, body, callback);
}

int RequestDelAsync(char *addr, int port, char *route, char *body, void (*callback)(int))
{
	RequestSendAsync(DELETE, addr, port, route, body, callback);
}

char *RequestSend(Methods method, CONST char *addr, int port, char *route, char *body)
{
	char *data;
	TCPSOCKET *sock;
	FILE *stream;
	LogMsg_P(LOG_INFO, PSTR("Connecting to server..."));

	sock = Connect(addr, port, route);
	if (sock == 0)
	{
		return -1;
	}
	LogMsg_P(LOG_INFO, PSTR("Connected to server!\nAttempting to write HTTP request to server...\n"));

	stream = _fdopen((int)sock, "r+b");

	// ---- Start HTTP Get request ---- //
	fprintf(stream, "GET %s HTTP/1.0\r\n", route);
	fprintf(stream, "Host: %s\r\n", addr);
	fprintf(stream, "User-Agent: IMC\r\n");
	fprintf(stream, "Connection: close\r\n\r\n");
	fflush(stream);
	// ---- End HTTP Get request ---- //

	LogMsg_P(LOG_INFO, PSTR("Sent HTTP request to server!"));

	LogMsg_P(LOG_INFO, PSTR("Downloading response from server..."));
	data = (char *)malloc(512 * sizeof(char));

	while (fgets(data, 512, stream))
	{
		if (0 == *data)
		break;

		printf("%s", data);
	}
	
	LogMsg_P(LOG_INFO, PSTR("Finished downloading!"));
	fclose(stream);

	return data;
}

char *RequestGet(char *addr, int port, char *route, char *body)
{
	return RequestSend(GET, addr, port, route, body);
}

char *RequestPost(char *addr, int port, char *route, char *body)
{
	return RequestSend(POST, addr, port, route, body);
}

char *RequestDel(char *addr, int port, char *route, char *body)
{
	return RequestSend(DELETE, addr, port, route, body);
}