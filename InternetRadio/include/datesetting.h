/*
 * datesetting.h
 *
 * Created: 21/02/2019 11:45:13
 * Author: Ian
 */ 

#ifndef DATESETTING_H_
#define DATESETTING_H_


/*
	Sets the value for the current day, month and year to 0
		and resets the cursor position.
*/
void InitDateSettings(void);

/*
	Increases the currently selected index of the date.
*/
void DateRight(void);

/*
	Decreases the currently selected index of the date.
*/
void DateLeft(void);

/*
	Increases the value of the currently selected number.
*/
void DateUp(void);

/*
	Decreases the value of the currently selected number.
*/
void DateDown(void);

/*
	Sets the currently selected digit.
*/
void SetCursor(void);

/*
	Changes the second line to the selected date.
*/
void SetDateScreen(void);

/*
	Retrieves the current year.
*/
int GetYear(void);

/*
	Retrieves the current month.
*/
int GetMonth(void);

/*
	Retrieves the current day.
*/
int GetDay(void);

/*
	Changes the date of the alarm to the currently set values.
*/
void ChangeDate(void);


#endif /* DATESETTING_H_ */
