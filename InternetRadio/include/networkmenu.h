/*
 * networkmenu.h
 *
 * Created: 19/03/2019 18:01:01
 * Author: Sergen
 */ 

#ifndef NETWORKMENU_H_
#define NETWORKMENU_H_


/*
	Initializes the possible network menu items.
*/
void InitNetworkMenuElements(void);

/*
	Initializes the values for the MAC address.
*/
void InitMacSettings(void);

/*
	Initializes the possible time synchronization items.
*/
void InitTimeSync(void);

/*
	Changes the currently selected item by increasing the current index.
*/
void NetworkMenuLeft(void);

/*
	Changes the currently selected item by decreasing the current index.
*/
void NetworkMenuRight(void);

/*
	Moves the cursor to the left.
*/
void MacLeft(void);

/*
	Moves the cursor to the right.
*/
void MacRight(void);

/*
	Moves the value of the currently selected value up by 1.
*/
void MacUp(void);

/*
	Moves the value of the currently selected value down by 1.
*/
void MacDown(void);

/*
	Changes the currently selected item by increasing the current index.
*/
void TimeSyncMenuLeft(void);

/*
	Changes the currently selected item by decreasing the current index.
*/
void TimeSyncMenuRight(void);

/*
	Executes a function based on the currently selected item.
*/
void SelectNetworkItem(void);

/*
	Sets the second line to the selected network menu element.
*/
void SetNetworkMenuElement(void);

/*
	Sets the second line to the selected time synchronization element.
*/
void SetTimeSyncMenuElement(void);

/*
	Sets the second line to the correct filled in characters of the MAC address.
*/
void SetMacScreen(void);

/*
	Updates the current value of the MAC address to the MAC address filled in.
*/
void ChangeMacAddress(void);

/* 
	Returns the MAC address in the format of 6 unsigned ints that are two hexadecimal numbers each.
	
	@param macAdress - The unsigned integer where the MAC address will be filled into.
*/ 
void GetMACAddress(unsigned int *);

/*
	Turns the time synchronization on/off based on the selected item.
*/
void TimeSyncConfirm(void);


#endif /* NETWORKMENU_H_ */
