/*
 * settings.h
 *
 * Created: 21/02/2019 11:10:45
 * Author: Ian
 */ 

#ifndef SETTINGS_H_
#define SETTINGS_H_


/*
	Initializes the settings menu.
*/
void InitSettings(void);

/*
	Changes the currently selected item by increasing the current index.
*/
void SettingsRight(void);

/*
	Changes the currently selected item by decreasing the current index.
*/
void SettingsLeft(void);

/*
	Changes the currently selected item by increasing the current index.
*/
void TimezonesRight(void);

/*
	Changes the currently selected item by decreasing the current index.
*/
void TimezonesLeft(void);

/*
	Sets the values for all the settings options.
*/
void InitSettingsElements(void);

/*
	Sets the currently selected element as the second line of the screen.
*/
void SetSettingsMenuElement(void);

/*
	Executes a function based on the currently selected item.
*/
void SelectSettingsItem(void);

/*
	Updates the timezone based on the currently selected timezone.
*/
void SelectTimezoneItem(void);

/*
	Changes the current state to factory reset and draws the matching screen.
*/
void ToFactoryReset(void);

/*
	Resets the alarms to 00:00 in non-volatile memory.
	NOTE: MAC address is not reset, because this would cause problems with the functioning of the networking
*/
void FactoryReset(void);


#endif /* SETTINGS_H_ */
