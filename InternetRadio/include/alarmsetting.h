/*
 * alarmsetting.h
 *
 * Created: 28-2-2019 11:53:16
 * Author: Ricky
 */ 

#ifndef ALARMSETTING_H_
#define ALARMSETTING_H_


/*
	Initializes the alarm settings .
*/
void InitAlarmSetting(void);

/*
	Moves the indication cursor 1 space to the left.
*/
void AlarmSettingLeft(void);

/*
    Moves the indication cursor 1 space to the right.
*/
void AlarmSettingRight(void);

/*
    Increases the indicated number with 1.
*/
void AlarmSettingUp(void);

/*
    Decreased the indicated number with 1.
*/
void AlarmSettingDown(void);

/*
    Sets the alarm time and returns to the menu.
*/
void AlarmSettingConfirm(void);


#endif /* ALARMSETTING_H_ */
