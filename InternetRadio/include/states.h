/*
 * states.h
 *
 * Created: 18/02/2019 20:05:35
 * Author: Ian
 */

#ifndef STATES_H_
#define STATES_H_


#define IMC_STATE_OFF 0
#define IMC_STATE_MAIN 1
#define IMC_STATE_MENU 2
#define IMC_STATE_IDLE 3
#define IMC_STATE_ALARM 4
#define IMC_STATE_MENU_TIME 5
#define IMC_STATE_MENU_DATE 6
#define IMC_STATE_MENU_SETTINGS 7
#define IMC_STATE_MENU_ALARM_TIME 8
#define IMC_STATE_MENU_NETWORK_MAC 9
#define IMC_STATE_MENU_ALARM 10
#define IMC_STATE_MENU_ALARM_REARM 11
#define IMC_STATE_MENU_ALARM_SOUND 12
#define IMC_STATE_MENU_AUDIO 13
#define IMC_STATE_MENU_SETTINGS_FACTORY 14
#define IMC_STATE_MENU_ALARM_RADIO 15
#define IMC_STATE_MENU_NETWORK 16
#define IMC_STATE_MENU_ALARM_WEEK 17 // Week alarm menu
#define IMC_STATE_MENU_ALARM_WEEK_DAY 18
#define IMC_STATE_MENU_ALARM_MODE_SELECTION 19 // Normal Alarm or Week alarm
#define IMC_STATE_MENU_NETWORK_TIMESYNC 20
#define IMC_STATE_MENU_SETTINGS_TIMEZONE 21
#define IMC_STATE_MENU_AUDIO_VOLUME 22
#define IMC_STATE_MENU_AUDIO_TREBLE 26
#define IMC_STATE_MENU_AUDIO_BASS 27


/*
	Initializes the state system by defaulting it to the MAIN state
		and mapping the buttons based on this state.
*/
void InitState(void);

/*
	Sets the current state.
	
	@param state - The new state.
*/
void SetState(int);

/*
	Gets the current state.
*/
int GetState(void);


#endif /* STATES_H_ */
