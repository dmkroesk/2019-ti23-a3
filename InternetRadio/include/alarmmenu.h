/*
 * alarmmenu.h
 *
 * Created: 28-2-2019 15:59:06
 * Author: Ricky
 */ 

#ifndef ALARMMENU_H_
#define ALARMMENU_H_


/*
	Changes the currently selected item by increasing the current index.
*/
void AlarmMenuRight(void);

/*
	Changes the currently selected item by decreasing the current index.
*/
void AlarmMenuLeft(void);

/*
	Sets the values for all the settings options.
*/
void InitAlarmMenuElements(void);

/*
	Sets the currently selected element as the second line of the screen.
*/
void SetAlarmMenuElement(void);

/*
	Selects the current alarm menu item.
*/
void SelectAlarmItem(void);

/*
    Sets the values for all the settings options.
*/
void InitAlarmRadioMenuElements(void);

/*
    Selects the current radio menu item.
*/
void SelectRadioItem(void);

/*
    Changes the currently selected item by increasing the current index.
*/
void AlarmRadioMenuRight(void);

/*
    Changes the currently selected item by decreasing the current index.
*/
void AlarmRadioMenuLeft(void);

/*
    Sets the currently selected element as the second line of the screen.
*/
void SetAlarmRadioMenuElement(void);

/*
	Sets the re-arm on or off.
*/
void ReArmOnOff(void);

/*
	Confirms the sound or the re-arm settings.
*/
void ItemConfirm(void);

/*
	Sets the screen to 'Aan' or 'uit'.
*/
void SetReArmScreen(void);

/*
	Sets the screen to 'Beep' or 'Radio'.
*/
void SetSoundScreen(void);

/*
	Sets the radio on or off.
*/
void RadioOnOff(void);


#endif /* ALARMMENU_H_ */
