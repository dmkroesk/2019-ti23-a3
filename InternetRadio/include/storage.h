/*
 * storage.h
 *
 * Created: 26-2-2019 15:07:18
 * Author: Ian
 */ 

#ifndef STORAGE_H_
#define STORAGE_H_


#include "alarm.h"
#include "netcomponent.h"


typedef struct {	
	int data[12];
} Flash_MAC;

typedef struct {
	int HH;
	int MN;
	int alarmStatus;
} Flash_Alarm;

typedef struct {
	timezones timezone;
	int synched;
	int isDST;
} Flash_TimeSync;

typedef struct {
	Flash_Alarm alarms[7];
} Flash_WeekAlarm;

typedef struct{
	int volume;
} Flash_Volume;

typedef struct{
	int bass;
} Flash_Bass;

typedef struct{
	int treble;
} Flash_Treble;


/*
	Initializes the flash storage.
*/
void InitFlashStorage(void);

/*
	Resets the values in memory to the default.
*/
void ResetFlashStorage(void);

/*
	Writes the MAC address to the flash storage.

	@param mac - The mac address.
*/
void WriteMACToFlash(Flash_MAC);

/*
	Reads and returns the MAC address from the flash storage (see format above).
	
	@param mac - The Flash_MAC object stored into the flash memory.
*/
void ReadMACFromFlash(Flash_MAC *);

/*
	Writes the alarm to the flash storage.
	
	@param alarm - The alarm.
*/
void WriteAlarmToFlash(Flash_Alarm);

/*
	Reads the alarm from the flash storage.
	
	@param alarm - The Flash_Alarm object stored into the flash memory.
*/
void ReadAlarmFromFlash(Flash_Alarm *);

/*
	Writes the timeSync to the flash storage.
	
	@param timeSync - The timeSync.
*/
void WriteTimeSyncToFlash(Flash_TimeSync);

/*
	Reads the timeSync from the flash storage.
	
	@param timeSync - The Flash_TimeSync object stored into the flash memory.
*/
void ReadTimeSyncFromFlash(Flash_TimeSync *);

/* 
	Write the week alarm (1 alarm per day) to the flash storage 
	 
	@param weekAlarm - The week alarm 
*/ 
void WriteWeekAlarmToFlash(Flash_WeekAlarm weekAlarm); 
 
/* 
	Read the week alarm from the flash storage 
	 
	@param weekAlarm - The Flash_weekAlarm object that flash memory gets stored into 
*/ 
void ReadWeekAlarmFromFlash(Flash_WeekAlarm *weekAlarm);

/*
	Returns if the synchronization is turned on/off.
*/
int GetSyncStatus(void);

/*
	Tests saving and reading a struct.
	returns a string of the values of the struct.
	[Note] NEEDS TO BE DELETED!!!
*/
char *TestStorage(void);

/* 
	Writes the bass to the flash storage 
	 
	@param bass - The Bass 
*/ 
void WriteBassToFlash(Flash_Bass bass); 
 
/* 
	Writes the bass to the flash storage 
	 
	@param bass - The Bass 
*/ 
void ReadBassFromFlash(Flash_Bass *bass); 
 
/* 
	Writes the treble to the flash storage 
	 
	@param treble - The treble 
*/ 
void WriteTrebleToFlash(Flash_Treble treble); 
 
/* 
	Writes the treble to the flash storage 
	 
	@param treble - The treble 
*/ 
void ReadTrebleFromFlash(Flash_Treble *treble); 


#endif /* STORAGE_H_ */
