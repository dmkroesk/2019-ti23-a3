/*
* apiHandler.h
*
* Created: 3/20/2019 7:11:01 PM
* Author: Ruben
*/

#ifndef APIHANDLER_H_
#define APIHANDLER_H_


/*
time_hour is an int describing the hour.
time_minute is an int describing the minutes.
date_year is an int describing the year.
date_month is an int describing the month;
date_day is an int describing the day;
timezone is an int describing the timezone (ie. UTC +1 for amsterdam would be 1)
timesync is an int (0 or 1) indicating whether NTP sync should be on. 0 is off, 1 is on.
*/
typedef struct
{
    int time_hour;
    int time_minute;
    int date_year;
    int date_month;
    int date_day;
    int timezone;
    int timesync;
} DateTime_Settings;
/*
volume is an int describing the volume level.
treble is an int describing the treble level.
bass is an int describing the bass level.
*/
typedef struct
{
    int volume;
    int treble;
    int bass;
} Audio_Settings;
/*
ip_address is a char* (string) describing the IP address of a radio station. (ie. 123.123.123.123)
port is an int describing the port number of the radio station.
route is an char* (string) describing the route after the IP address.
*/
typedef struct
{
    char *ip_address;
    char *channel_name;
    int port;
    char *route;
} Radio_Settings;
/*
time_hour is an int describing the hour of the alarm.
time_minute is an int describing the minutes alarm.
re_arm is an int (0 or 1) describing whether the alarm should auto re_arm.
audio_type is an int (0 or 1) describing whether the alarm should play a beep sound (0) or play a radio channel (1)
radio_channel is a char* (string) describing the radio channel that should play when the alarm goes off if audio_type is 1.
*/
typedef struct
{
    int time_hour;
    int time_minute;
    int re_arm;
    int audio_type;
    int radio_channel;
} Alarm_Settings;
/*
factory_settings is an int (0 or 1) describing whether the IMC should default back to factory settings. (ie. 0 is do nothing, 1 is reset)
*/
typedef struct
{
    int factory_settings;
} General_Settings;
/*
ApiConfig aggregates all structs above into 1 struct resembling the JSON object returned from the server.
*/
typedef struct
{
    char *id;
    DateTime_Settings dt_settings;
    Audio_Settings audio_settings;
    Radio_Settings radio_settings;
    Alarm_Settings alarm_settings;
    General_Settings general_settings;
} ApiConfig;

/*
RequestConfig creates an HTTP GET request to the config api server.
After the function received a response it will attempt to convert it to JSON tokens.
These JSON tokens will then be used to create an ApiConfig struct.
If the struct is successfully created it will apply the Config to the IMC.
return 0 if no error occurred. If something goes wrong a negative integer will be returned. Use GetApiError error to get a human readable error.
*/
int RequestConfig();

/*
CreateConfigFromTokens returns an ApiConfig pointer.
@param tokens is an array containing all decoded JSON tokens.
@param length is the length of the tokens array.
*/
ApiConfig *CreateConfigFromString(char *json_content, int json_length);

/*
ApplyConfig applies the given config then frees the ApiConfig returning memory to the system.
*/
int ApplyConfig(ApiConfig *cfg);

/*
	FreeApiConfig frees all malloced items in the ApiConfig struct.
	@param cfg - Is the apiconfig pointer.
*/
int FreeApiConfig(ApiConfig *cfg);

/*
GetApiError is used to get a human readable error from an error code.
@param error_code is the error code as a negative number.
*/
char *GetApiError(int error_code);

/*
    PrintConfig prints the given config to the log.
    @param cfg is the ApiConfig pointer of the config.
*/
void PrintConfig(ApiConfig *cfg);

#endif /* APIHANDLER_H_ */