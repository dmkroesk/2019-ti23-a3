/* ========================================================================
* [PROJECT]    SIR100
* [MODULE]     Display
* [TITLE]      display header file
* [FILE]       display.h
* [VSN]        1.0
* [CREATED]    030414
* [LASTCHNGD]  030414
* [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
* [PURPOSE]    API and gobal defines for display module
* ======================================================================== */

#ifndef _Display_H
#define _Display_H


#define DISPLAY_SIZE 16
#define NROF_LINES 2
#define MAX_SCREEN_CHARS (NROF_LINES * DISPLAY_SIZE)

#define LINE_0 0
#define LINE_1 1

#define FIRSTPOS_LINE_0 0
#define FIRSTPOS_LINE_1 0x40

#define LCD_CURSOR_LEFT 0
#define LCD_CURSOR_RIGHT 1

#define LCD_BACKLIGHT_ON 1
#define LCD_BACKLIGHT_OFF 0

#define LCD_BACKLIGHT_STATE_ON 1
#define LCD_BACKLIGHT_STATE_OFF 0

#define LCD_CURSOR_BLINK_ON 1
#define LCD_CURSOR_BLINK_OFF 0

#define ALL_ZERO 0x00      // 0000 0000 B
#define WRITE_COMMAND 0x02 // 0000 0010 B
#define WRITE_DATA 0x03    // 0000 0011 B
#define READ_COMMAND 0x04  // 0000 0100 B
#define READ_DATA 0x06     // 0000 0110 B
#define DISPLAY_LENGTH 80


/*
	Turns the backlight on/off.
	
	@param Mode - Value which indicates if the backlight should be turned on/off.
*/
extern void LcdBackLight(u_char);

/*
	Writes a single character on the LCD on the current cursor position.
	
	@param MyChar - Character to write.
*/
extern void LcdChar(char);

/*
	Initialise the controller and send the User-Defined Characters to CG-RAM
		settings: 4-bit interface, cursor invisible and NOT blinking
				  1 line dislay, 10 dots high characters
*/
extern void LcdLowLevelInit(void);

/*
	Initialises lcd.
	Note: Doesn't exist.
*/
extern void LcdInit(void);

/*
	Draws the internet icon in the top-right corner of the lcd screen.
*/
extern void LcdDrawInternetIcon(void);

/*
	Draws the alarm icon in the bottom-right corner of the lcd screen.
*/
extern void LcdDrawAlarmIcon(void);

/*
	Draws the synchronize icon in the top-left corner of the lcd screen.
*/
extern void LcdDrawSyncIcon(void);

/*
	Writes a string on the lcd screen on a chosen line.
		
	@param *s - The string that's going to be written.
	@param line - Indication on which line/row the string will appear.
*/
extern void LcdWriteString(char*, int);

/*
	Clears the lcd screen.
*/
extern void LcdClear(void);

/*
	Shifts the cursor a chosen amount of positions in a chosen direction.
		
	@param direction - The shift direction.
	@param amount - The shift amount.
*/
extern void LcdShiftCursor(int, int);

/*
	Shifts the display a chosen amount of positions in a chosen direction.

	@param direction - The shift direction.
	@param amount - The shift amount.
*/
extern void LcdShiftDisplay(int, int);

/*
	Creates the physical manifestation of the visitor on the map.
		
	@param index The size of the characters array.
	@param characters An array of characters.
	@param album A struct of the type Album.
*/
extern void LcdSetCursor(int, int);

/*
	Sets the cursor on a chosen line and position.

	@param line - Indication on which line/row the cursor will appear.
	@param position - Indication at what position the cursor will appear.
*/
extern void LcdSetCursorPosition(int, int);

/*
	Turns the cursor blink state on/off, simultaniously turning on/off the cursor.
		
	@param state - The state to change to.
*/
extern void LcdSetCursorBlinkState(int);


#endif /* _Display_H */
