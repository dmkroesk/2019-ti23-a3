/*
 * timesetting.h
 *
 * Created: 02/21/2019 11:46:10 AM
 * Author: Ruben
 */

#ifndef TIMESETTING_H_
#define TIMESETTING_H_


/*
	Sticks 2 number sets together.
	
	@param x - Left set of numbers to stick together.
	@param y - Right set of numbers to stick together.
*/
unsigned Concatenate(unsigned, unsigned);

/*
	Moves the indication cursor 1 space to the left.
*/
void TimeSettingLeft(void);

/*
    Moves the indication cursor 1 space to the right.
*/
void TimeSettingRight(void);

/*
    Increases the indicated number with 1.
*/
void TimeSettingUp(void);

/*
    Decreased the indicated number with 1.
*/
void TimeSettingDown(void);

/*
    Sets the time in the RTC chip and returns to the menu.
*/
void TimeSettingConfirm(void);


#endif /* TIMESETTING_H_ */
