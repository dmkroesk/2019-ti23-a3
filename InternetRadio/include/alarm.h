/*
 * alarm.h
 *
 * Created: 21/02/2019 11:21:44
 * Author: Boudewijn
 */ 

#ifndef ALARM_H_
#define ALARM_H_


#include "time.h"
#include "storage.h"


typedef struct {
	// Y2K(century(20)), DW(day week), YR(year),MO(month), DT(day), HR(hour), MN(min)		
	int Y2K, DW, YR, MO,  DT,  HR,  MN, Sec;
} Time;

typedef struct {
	int status, snooze, ReArm, Radio;
	Time alarmTime;
} Alarm;


/*
	Turns the alarm off, by rearming it and calling VsBeepStop function.
	
	[TODO] Make sure AlarmDone(); works this one is in keylogic.c
*/
void AlarmOff(void);

/*
	Set the alarm time on the RTC chip.
	
	[TODO] Make sure the alarm is saved locally it can be found in git history
	
	@param *tm - The time the alarm needs to be set to.
	@param HR - The hour the alarm needs to be set to.
	@param MN - The minute the alarm needs to be set to.
*/
void SetAlarmTime(/*struct _tm * */ int,int);

/*
	Rings the alarm and let's it beep, which also changes it's state.
*/
void Ring(void);

/*
	Creates a thread to check if the alarm is occurred. This needs to be changed so it polls with the method from the RTC chip.
	
	@param *tm - Gives a out parameter for the current time that is set for the alarm.
*/
void GetAlarmTime(tm *);

/*
	Switches the state of the alarm.
*/
void OnOffSwitch(void);

/*
	Re-arms the alarm on the RTC chip.
	
	[TODO] Needs to make sure the local alarm is updated.
*/
void Re_Arm(void);

/*
	Makes sure the IPAC will check for the alarm.
*/
void AlarmCheckOn(void);

/*
	Returns the status of the alarm.
*/
int GetAlarmStatus(void);

/*
	The Alarm will ring again in 2 minutes.
*/
void Snooze(void);

/*
	Initalize the alarm with the standerd of 2019, 1 jan, 00:00.
*/
void AlarmInit(void);

/*
	Returns if the re-arm is on.
*/
int GetReArm(void);

/*
	Sets the re-arm off.
*/
void SetReArmOff(void);

/*
	Sets the re-arm on.
*/
void SetReArmOn(void);

/*
	Returns the radio state.
*/
int GetRadio(void);

/*
	Sets the radio off.
*/
void SetRadioOff(void);

/*
	Sets the radio on.
*/
void SetRadioOn(void);

/*
	Checks if the current ringing alarm should automatically be stopped (which is after 3 minutes).
	
	@param *tm - Gives a out parameter for the current time.
*/
int CheckAutoAlarmDone(tm *);

/*
	Returns whether the alarm is going off at the moment.
*/
int GetAlarmOn(void);

/*
	Turns off the alarmOn notifier.
*/
void SetAlarmOff(void);

/*
	Determines which alarm is the closest to the current time based on the day of the week
*/
void DetermineNearestAlarm(void);

/*
	Updates the local weekAlarm values
	
	@param weekAlarm - The current weekAlarm object
*/
void UpdateWeekAlarm(Flash_WeekAlarm);


#endif /* ALARM_H_ */
