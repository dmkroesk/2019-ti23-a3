/*
* keybinding.h
*
* Created: 18/02/2019 20:00:32
* Author: Ian
*/

#ifndef KEYBINDING_H_
#define KEYBINDING_H_


#define LOG_MODULE  LOG_MAIN_MODULE

#include <stdio.h>
#include <string.h>
#include <avr/delay.h>


/*
	The key is the received input, and the value is the function that has to be executed as a result.
*/
struct Pair {
	u_char key;
	void(*value)(void);
};


/*
	Sets the initial values for the buttons.
*/
void InitButtons(void);

/*
	Sets the values of the buttons based on the current state.
	
	@param state - The current state.
*/
void SetButtons(int state);

/*
	Returns the function that should be executed based on the key that is pressed.

	@param key - The key that is pressed.
*/
void *GetValue(u_char key);


#endif /* KEYBINDING_H_ */
