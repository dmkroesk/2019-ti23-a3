/*
 * datetime.h
 *
 * Created: 19/02/2019 14:56:50
 * Author: Sergen
 */ 

#ifndef DATETIME_H_
#define DATETIME_H_


#include "time.h"


/*
	ISR MainBeat Timer Interrupt (Timer 2 for Mega128, Timer 0 for Mega256).
		This routine is automatically called during system initialization.
		Resolution of this Timer ISR is 4,448 msecs.

	@param *p - Not used (might be used to pass parms from the ISR)
*/
void SysMainBeatInterrupt(void *);

/*
	Initialise Digital IO.
		Init inputs to '0', outputs to '1' (DDRxn='0' or '1')
		Pull-ups are enabled when the pin is set to input (DDRxn='0') and then a '1'
		is written to the pin (PORTxn='1')
*/
void SysInitIO(void);

/*
	Starts or stops the 4.44 msec mainbeat of the system.
 
	@param onOff - Indicates if the mainbeat needs to start or to stop.
*/
void SysControlMainBeat(u_char);

/*
	Set current time in x1205 RTC to the next day.
	
	@param DW - The current day of the week.
	@param YR - The current year.
	@param MO - The current month.
	@param DT - The current date of the month.
	@param HR - The current hour.
	@param MIN - The current minute.
*/
void SetNextDay(int, int, int, int, int, int);

/*
	Set current time in x1205 RTC to the previous day.
	
	@param DW - The current day of the week.
	@param YR - The current year.
	@param MO - The current month.
	@param DT - The current date of the month.
	@param HR - The current hour.
	@param MIN - The current minute.
*/
void SetPreviousDay(int, int, int, int, int, int);

/*
	Set current time in x1205 RTC.
	
	@param Y2K - Unknown.
	@param DW - The day of the week you want to set (range 0-6). 
	@param YR - The year you want to set (range 0-99).
	@param MO - The month you want to set ( range 1-12).
	@param DT - The date you want to set ( range 1-31).
	@param HR - The hours of the time you want to set (range 0-23).
	@param MN - The minutes of the time you want to set (range 0-59).
	@param SC - The seconds of the time you want to set (0-59).
*/
void SetX1205Time(int, int, int, int, int, int, int, int);

/*
	Sets a _tm struct, inserted into the method call, to the RTC time.
	
	@param *tm - A _tm struct that is set to the RTC time.
*/
void GetDateTime(tm *);

/*
	Sets the RTC time to a chosen time.
	
	@param HR - The hours of the time you want to set (range 0-23).
	@param MN - The minutes of the time you want to set (range 0-59).
*/
void DateTime_SetTime(int, int);

/*
	Sets the RTC date to a chosen date.
	
	@param YY - The year you want to set (range 0-99).
	@param MM - The month you want to set ( range 1-12).
	@param DD - The date you want to set ( range 1-31).
*/
void DateTime_SetDate(int, int, int);

/*
	Returns the date, which is stored in the RTC chip.
*/
char *DateTime_GetDate(void);

/*
	Returns the index of the current day of the week
*/
int GetDOW(void);

/*
	Returns the current seconds.
*/
int GetDateTimeSeconds(void);


#endif /* DATETIME_H_ */
