/*
* json_parser.h
*
* Created: 3/21/2019 3:24:52 PM
*  Author: Ruben
*/

#ifndef JSON_PARSER_H_
#define JSON_PARSER_H_


int ParseJSON(char *data, char *leftDelimiter, char *rightDelimiter, char **out);

char *find_string_value_in_string(char *key, char *str, int length);

int find_int_value_in_string(char *key, char *str, int length);

#endif /* JSON_PARSER_H_ */