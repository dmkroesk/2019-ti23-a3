/*
 * keylogic.h
 *
 * Created: 18/02/2019 20:03:46
 * Author: Ian
 */ 

#ifndef KEYLOGIC_H_
#define KEYLOGIC_H_


#define NUMBER_OF_STRINGS 5


/*
	Turns the internet radio off and on again very quick.
*/
void TurnOff(void);

/*
	Goes to the main screen with date and a time.
*/
void ToMain(void);

/*
	Returns the screen to the menu and sets the last menu element. 
*/
void ToMenu(void);

/*
	Goes to the alarm menu settings.
*/
void ToAlarmMenu(void);

/*
	Goes to the alarm time settings.
*/
void ToAlarmTime(void);

/*
	Goes to the alarm rearm settings.
*/
void ToAlarmReArm(void);

/*
	Goes to the alarm sound settings.
*/
void ToAlarmSound(void);

/*
	Goes to the time menu settings. The user can input time here.
*/
void ToTime(void);

/*
	Goes to the date menu settings. The user can input date here.
*/
void ToDate(void);

/* 
    Goes to the alarm radio menu settings. The user can input date here.
*/ 
void ToAlarmRadio(void);

/*
	Go with the menu to the settings screen. 
*/
void ToSettings(void);

/*
	Goes to the network settings screen. 
*/
void ToNetwork(void);

/*
	Goes to the MAC settings screen. The user can input MAC-adres here.
*/
void ToMAC(void);

/*
	Goes to the time synchronization settings screen. The user can turn the synchronization on/off.
*/
void ToTimeSync(void);

/* 
	Goes to the menu with alarms for every day of the week 
*/ 

void ToWeekAlarm(void);

/* 
	Goes To the audio menu settings. The user can navigate to audio sub menus 
*/ 
void ToAudioMenu(void); 
 
/* 
	Goes To the bass sub-menu settings. The user can alter the bass here 
*/ 
void ToAudioBassMenu(void); 
 
/* 
	Goes To the tremble sub-menu settings. The user can alter the tremble here 
*/ 
void ToAudioTrebleMenu(void); 
 
/* 
	Goes To the volume menu settings.  
*/ 
void ToVolume(void); 

/*
	Sets menu 1 item to the left by decreasing menuElements by 1 and call the SetMenuElement();.
*/
void MenuLeft(void);

/*
	Sets menu 1 item to the right by raising menuElements by 1 and call the SetMenuElement();.
*/
void MenuRight(void);

/*
	Selects and navigates to current item.
*/
void SelectItem(void);

/*
	Initalize the items in the menuelements.
*/
void InitMenuElements(void);

/*
	Sets the screen with the current menu item that is selected.
*/
void SetMenuElement(void);

/*
	Snooze the alarm by calling the function in alarm.c.
*/
void AlarmSnooze(void);

/*
	Turns the alarm off.
	
	[TODO] fix this one!!!
*/
void AlarmDone(void);

/*
	Switches the alarm on/off, depending if it is already on/off.
*/
void SwitchAlarm(void);

/*
	Switches the radio on/off, depending if it is already on/off.
*/
void SwitchRadio(void);

/*
	Let the radio play music.
*/
void RadioOn(void);

/*
	Stops the radio.
*/
void RadioOff(void);

/*
	Display the Internet on icon.
*/
void InternetOn(void);

/*
	Display the Internet off icon.
*/
void InternetOff(void);

/*
	Set the settings.
*/
void SetSettings(void);

/*
	Set the menu.
*/
void SetMenu(void);

/*
	Awake the device and go to main();.
*/
void Awake(void);

/*
	Get the state of the alarm at the moment.
*/
int GetAlarmState(void);

/* 
    Navigates the channel to left.
*/ 
void SwitchChannelLeft(void); 
 
/* 
    Navigates the channel to the right.
*/ 

void SwitchChannelRight(void); 

/*  
    The bass will increase 
*/  
void BassUp(); 
 
/*  
    The bass will decrease 
*/  
void BassDown(); 
 
/*  
    The treble will increase 
*/  
void TrebleUp(); 
 
/*  
    The treble will decrease 
*/  
void TrebleDown(); 

/*  
    Navigates the channel to left  
*/  
void VolumeUp(void);  
  
/*  
    Navigates the channel to the right  
*/  
void VolumeDown(void);  


#endif /* KEYLOGIC_H_ */
