/*
 * streamplayer.h
 *
 * Created: 28/02/2019 08:57:57
 * Author: Boudewijn
 */

#ifndef EPICSTREAMPLAYER_H_
#define EPICSTREAMPLAYER_H_


#include <sys/nutconfig.h>
#include <sys/types.h>
#include <stdio.h>
#include <io.h>


/*
	Plays a stream.
	
	@param stream - Pointer to a stream of data.
*/
int PlayStream(FILE *);

/*
	Stops the stream.
*/
int StopStream(void);

/*
	Gets whether the buffering is done.
*/
int GetBufferState(void);


#endif /* EPICSTREAMPLAYER_H_ */
