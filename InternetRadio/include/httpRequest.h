
/*
* httpRequest.h
*
* Created: 3/19/2019 1:35:51 PM
*  Author: Ruben
*/

#ifndef HTTPREQUEST_H_
#define HTTPREQUEST_H_

#include <sys/thread.h>

#include <stdio.h>
#include <string.h>

#include "log.h"
#include "netcomponent.h"

typedef enum
{
	GET = 0,
	POST = 1,
	DELETE = 2
} Methods;

/*
RequestSendAsync creates a Thread which will send an http request and receive data back.
After receiving the data the thread will call the callback and exit.
@param method is an enum on type Methods (includes GET, POST, DELETE).
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
@param callback is a function thats called when the request is done.
*/
int RequestSendAsync(Methods method, char *addr, int port, char *route, char *body, void (*callback)(int));
/*
Abstraction over RequestSendAsync to more easily send a get request.
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
@param callback is a function thats called when the request is done.
*/
int RequestGetAsync(char *addr, int port, char *body, char *route, void (*callback)(int));
/*
Abstraction over RequestSendAsync to more easily send a post request.
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
@param callback is a function thats called when the request is done.
*/
int RequestPostAsync(char *addr, int port, char *body, char *route, void (*callback)(int));
/*
Abstraction over RequestSendAsync to more easily send a delete request.
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
@param callback is a function thats called when the request is done.
*/
int RequestDelAsync(char *addr, int port, char *body, char *route, void (*callback)(int));

/*
sendRequest sends a request to the server and the blocks until its done.
@param method is an enum on type Methods (includes GET, POST, DELETE).
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
*/
char *RequestSend(Methods method, CONST char *addr, int port, char *route, char *body);

/*
Abstraction over sendRequest to more easily send a get request.
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
*/
char *RequestGet(char *addr, int port, char *route, char *body);

/*
Abstraction over sendRequest to more easily send a post request.
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
*/
char *RequestPost(char *addr, int port, char *route, char *body);

/*
Abstraction over sendRequest to more easily send a delete request.
@param addr is the address of the api server.
@param port is the port number of the api server.
@param body is the body for the request.
*/
char *RequestDel(char *addr, int port, char *route, char *body);

#endif /* HTTPREQUEST_H_ */