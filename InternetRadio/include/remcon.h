/* ========================================================================
 * [PROJECT]    SIR
 * [MODULE]     Remote Control
 * [TITLE]      remote control header file
 * [FILE]       remcon.h
 * [VSN]        1.0
 * [CREATED]    1 july 2003
 * [LASTCHNGD]  1 july 2003
 * [COPYRIGHT]  Copyright (C) STREAMIT BV 2010
 * [PURPOSE]    remote control routines for SIR
 * ======================================================================== */

/*-------------------------------------------------------------------------*/
/* global defines                                                          */
/*-------------------------------------------------------------------------*/
#define RC_OK                 0x00
#define RC_ERROR              0x01
#define RC_BUSY               0x04

#define RCST_IDLE             0x00
#define RCST_WAITFORLEADER    0x01
#define RCST_SCANADDRESS      0x02
#define RCST_SCANDATA         0x03

#define RC_INT_SENS_MASK      0x03
#define RC_INT_FALLING_EDGE   0x02
#define RC_INT_RISING_EDGE    0x03

#define IR_RECEIVE            4
#define IR_BUFFER_SIZE        1


extern const char REMOTE_BUTTON_ONE[];
extern const char REMOTE_BUTTON_TWO[];
extern const char REMOTE_BUTTON_THREE[];
extern const char REMOTE_BUTTON_FOUR[];
extern const char REMOTE_BUTTON_FIVE[];
extern const char REMOTE_BUTTON_ALt[];
extern const char REMOTE_BUTTON_ESCAPE[];
extern const char REMOTE_BUTTON_UP[];
extern const char REMOTE_BUTTON_OK[];
extern const char REMOTE_BUTTON_LEFT[];
extern const char REMOTE_BUTTON_DOWN[];
extern const char REMOTE_BUTTON_RIGHT[];
extern const char REMOTE_BUTTON_POWER[];


/*-------------------------------------------------------------------------*/
/* export global routines (interface)                                      */
/*-------------------------------------------------------------------------*/
void RcInit(void);

/*
	Starts a function when the remote is pressed.
	@param button - The button that is pressed
*/
void StartRemoteFunction(char *);

/*
	Returns the pressed button.
*/
int GetRemoteButton(void);

/*
	Sets bitsFull to 0.
*/
void ResetBits(void);

/*
	Returns 1 if bits has over 32 characters, else 0.
*/
int bitsReady(void);

/*
	Returns a char array with bits from the remote.
*/
char * getBits(void);

/*  様様  End Of File  様様様様 様様様様様様様様様様様様様様様様様様様様様様 */
