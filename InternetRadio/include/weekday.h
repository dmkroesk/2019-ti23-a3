/* 
 * weekday.h
 * 
 * Created: 19-3-2019 17:01:08
 * Author: Ian
 */

#ifndef WEEKDAY_H_
#define WEEKDAY_H_


/*
	Initializes the week day screen.
	
	@param day - The currently selected day.
*/
void InitWeekDay(int day);

/*
	Moves the cursor to the left
*/
void Weekday_Left(void);

/*
	Moves the cursor to the right.
*/
void Weekday_Right(void);

/*
	Increases the value of the currently selected element.
*/
void Weekday_Up(void);

/*
	Decreases the value of the currently selected element.
*/
void Weekday_Down(void);

/*
	Sets the cursor of the week day screen.
*/
void SetWeekDayCursor(void);

/*
	Sets the second line and then draws the week day screen.
*/
void SetWeekDayScreen(void);

/*
	Returns the current hours in HH format.
*/
int GetHours(void);

/*
	Returns the current minutes in MM format.
*/
int GetMinutes(void);

/*
	Adjusts one of the current week day alarms and stores it in flash memory, then moves back to the alarm menu.
*/
void ConfirmWeekDaySettings(void);


#endif /* WEEKDAY_H_ */
