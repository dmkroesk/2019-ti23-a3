/* 
 * weekalarms.h
 * 
 * Created: 19-3-2019 15:14:07
 * Author: Ian
 */

#ifndef WEEKALARMS_H_
#define WEEKALARMS_H_


/*
	Initializes the week alarm menu.
*/
void InitWeekAlarmMenu(void);

/*
	Initializes the elements of the week alarm menu.
*/
void InitWeekAlarmElements(void);

/*
	Sets the currently selected week alarm element.
*/
void SetWeekAlarmElement(void);

/*
	Moves the cursor to the left.
*/
void WeekAlarm_Left(void);

/*
	Moves the cursor to the right.
*/
void WeekAlarm_Right(void);

/*
	Moves to the currently selected item.
*/
void WeekAlarm_SelectItem(void);


#endif /* WEEKALARMS_H_ */
