/*
* netcomponent.h
*
* Created: 26/02/2019 14:50:28
* Author: Boudewijn
*/

#ifndef NETTERTET_H_
#define NETTERTET_H_


#define OK 0
#define NOK -1
#define ETH0_BASE 0xC300
#define ETH0_IRQ 5
#define RADIO_PORT 8465

#include <sys/thread.h>
#include <sys/timer.h>
#include <dev/irqreg.h>
#include <dev/nicrtl.h>
#include <arpa/inet.h>
#include <sys/confnet.h>
#include <pro/dhcp.h>
#include <sys/socket.h>
#include <pro/sntp.h>
#include <stdio.h>

#include "radio.h"


typedef enum {
	UTC_MIN_12,UTC_MIN_11,UTC_MIN_10,UTC_MIN_9_30,UTC_MIN_9,UTC_MIN_8,UTC_MIN_7,UTC_MIN_6,UTC_MIN_5,UTC_MIN_4,UTC_MIN_3_30,UTC_MIN_3,
	UTC_MIN_2_30,UTC_MIN_2,UTC_MIN_1,UTC,UTC_PLUS_1,UTC_PLUS_2,UTC_PLUS_3,UTC_PLUS_3_30,UTC_PLUS_4,UTC_PLUS_4_30,UTC_PLUS_5,UTC_PLUS_5_30,
	UTC_PLUS_5_45,UTC_PLUS_6,UTC_PLUS_6_30,UTC_PLUS_7,UTC_PLUS_8,UTC_PLUS_8_45,UTC_PLUS_9,UTC_PLUS_9_30,UTC_PLUS_10,UTC_PLUS_10_30,
	UTC_PLUS_11,UTC_PLUS_12,UTC_PLUS_12_45,UTC_PLUS_13,UTC_PLUS_13_45,UTC_PLUS_14
} timezones;


/*
	Checks if it is daylight saving time or not.
	
	@param DT - Current day of the month.
	@param MO - Current month.
	@param DW - Current day of the week.
*/
int IsDST(int, int, int);

/*
	Start the initialization for the network connection by calling the "init_network" THREAD.

	@param callback - This is the function that should be executed when the network was initialised.
*/
void StartInitNetwork(void (*callback)(int));

/*
	Synchronizes the time with the selected timezone.
*/
int SyncTime(void);

/*
	Gets the NTP time from a timeserver and formats it using a timezone.

	@param addr - The ip-address of the NTP server.
*/
int GetNtpTime(char *);

/*
	Connects to a TCP server, which then returns a TCP socket.

	@param addr - IP-address for the stream. This is where the radio is hosted.
	@param port - Port number for the stream.
	@param route - Route after the index page of the stream. Format: "/<>/<>/..."
*/
TCPSOCKET *Connect(CONST char *, int, char *);

/*
	Opens the stream with a selected radio station.
	
	@param channel - The current radiochannel.
	@param route - Route after the index page of the stream. Format: "/<>/<>/..."
*/
int OpenStream(RadioChannel, char *);

int GetInternetStatus(void);

/*
	Returns the current streaming radio station.
*/
FILE *GetStream(void);


#endif /* NETTERTET_H_ */
