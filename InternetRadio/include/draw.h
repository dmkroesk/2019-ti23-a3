/*
* draw.h
*
* Created: 18-2-2019 20:07:41
* Author: Ian
*/

#ifndef DRAW_H_
#define DRAW_H_


#define LINELENGTH 16


/*
	Initializes the screen by calling the UpdateDateTime method.
*/
extern void InitScreen(void);

/*
	Updates the 2 lines, which are shown on the lcd screen, to the correct time and date.
*/
extern void UpdateDateTime(void);

/*
	Writes the 2 lines, saved in the draw.c module, on the lcd screen.
*/
extern void DrawScreen(void);

/*
	Sets line 1, saved in the draw.c module, which will eventually be drawn onto the screen.
	
	@param line - The string you want to eventually be drawn onto the screen.
*/
extern void SetScreenLine1(char *);

/*
	Draws the volume on the screen
	
	@param volume - The current volume level
*/
extern void DrawVolume(int volume);

/*
	Draws the bass on the screen
	
	@param bass - The current bass level
*/
extern void DrawBass(int bass);

/*
	treble the volume on the screen
	
	@param tremble - The string you want to eventually be drawn onto the screen.
*/
extern void DrawTremble(int tremble);

/*
	Sets line 2, saved in the draw.c module, which will eventually be drawn onto the screen.
	
	@param line - The string you want to eventually be drawn onto the screen.
*/
extern void SetScreenLine2(char *);

/*
	Returns the first line's text.
*/
extern char *GetScreenLine1(void);

/*
	Returns the second line's text.
*/
extern char *GetScreenLine2(void);


#endif /* DRAW_H_ */
