/*
 * radio.h
 *
 * Created: 15/03/2019 10:02:36
 * Author: Boudewijn
 */ 

#ifndef RADIO_H_
#define RADIO_H_


typedef struct {
    char *name;
    char *url;
    int port;
} RadioChannel;


/*
    Sets the current radioalarm to the channel.
    
    @param channel - The current channel for the radio alarm.
*/
void SetRadioAlarm(int channel);

/*
    Sets the current radioalarm to the channel.
*/
void StartMainRadio(void);

/*
    Starts the main radio.
*/
void StartAlarmRadio(void);

/*
    Starts the alarm radio.
*/
void InitRadioList(void);

/*
    Sets the current mainalarm to the channel.
    
    @param channel - The current channel for the main alarm.
*/
void SetMainRadio(int channel);

/*
    Returns the channel of the alarm.
*/
int GetMainChannel(void);

/*
    Get main name of the radio.
*/
char* GetNameMain(void);

/*
    Sets the status if it is playing.
*/
void SetPlaying(int status);

/*
    Gives the status if the radio is playing.
*/
int GetPlaying(void);

/*
    Adds a radio channel to the available radio channels.
	
	@param channel - The new channel that should be added.
*/
void AddRadioChannel(RadioChannel *);


#endif /* RADIO_H_ */
