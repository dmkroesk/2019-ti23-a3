/*
 * soundcontroller.h
 *
 * Created: 28/02/2019 11:18:29
 *  Author: Bou's Laptop
 */ 
#include <sys/nutconfig.h>
#include <sys/types.h>
#include <stdio.h>
#include <io.h>

#ifndef SOUNDCONTROLLER_H_
#define SOUNDCONTROLLER_H_

#define OK			1
#define NOK			0

/*
	playtone
	plays a tone melody
*/
void PlayTone();

/*
	initStream
	initialize the music stream
*/
int InitStream();

/*
	Kills the sound of both outputs.
*/
void SilentRadio(void);

/*
	Calculates the right volume
*/
int CalculateVolume(void);

/*
	Updates the volume to the new volume
	
	@param update - the number to let know if it needs to go down or up 1 up -1 down
*/
void UpdateVolume(int update);

/*
	Returns the current volume of the radio
*/
int GetVolume(void);

/*
	Returns the current bass of the radio
*/
int GetBass(void);

/*
	Returns the current treble of the radio
*/
int GetTreble(void);

/*
	Returns the bass register with new bass and treble
*/
int UpdateBassRegister(void);

/*
	Updates the treble to the new volume
	
	@param update - the number to let know if it needs to go down or up 1 up -1 down
*/
void UpdateTreble(int update);

/*
	Updates the bass to the new volume
	
	@param update - the number to let know if it needs to go down or up 1 up -1 down
*/
void UpdateBass(int update);

/*
	Init the audio menu items
*/
void InitAudioMenuElements(void);

/*
	MAkes sure the right item is on the screen
*/
void SetAudioMenuElement(void);

/*
	Selects the current selected menu item
*/
void SelectAudioItem(void);

/*
	Audio menu navigate left
*/
void AudioLeft(void);

/*
	Audio menu navigate right
*/
void AudioRight(void);

#endif /* SOUNDCONTROLLER_H_ */
